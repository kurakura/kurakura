<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Login</title>
        <link href="PresentationLayer/KendoUI/styles/kendo.common.min.css" rel="stylesheet">
        <link href="PresentationLayer/KendoUI/styles/kendo.default.min.css" rel="stylesheet">
        <link href="PresentationLayer/styles/style.css" rel="stylesheet">
        <link href="PresentationLayer/styles/styleOverride.css" rel="stylesheet">
        <link href="PresentationLayer/styles/styleSpecific.css" rel="stylesheet">
    
        <script src="PresentationLayer/KendoUI/js/jquery.min.js"></script>
        <script src="PresentationLayer/KendoUI/js/kendo.web.min.js"></script>
    </head>
    
    <body>                
		<table id="wrapper">
			<tr>
				<td>
                	<div class="loginForm">
                        <h1 class="loginTitle">Iniciar sesión</h1>
                        <div id="loginForm"class="form">       
                            <ul>
                                <li>
                                    <label class="required">Usuario</label>
                                        
                                    <input id="username" class="k-textbox" autofocus="autofocus" type="text" required validationMessage=" "/>
                                </li>
                                
                                <li>
                                    <label class="required">Contraseña</label>
                                                            
                                    <input id="password" type="password" class="k-textbox" required validationMessage=" " />
                                </li>
                                
                                 
                                <li class="status">
                                </li>
                
                                <li>
                                    <input id="loginButton" value="Iniciar Sesión" class="k-button" type="submit" />
                                </li>
                            </ul>
                    	</div>
                    </div>
				</td>
			</tr>
		</table>     
	</body>
    
	<script>
		 $(document).ready(function() {
			$("#loginButton").click(function() {
				var validator = $("#loginForm").kendoValidator().data("kendoValidator"),
				status = $(".status");
				if (validator.validate()) {
					$.getJSON('ServiceLayer/?access/login/' + $("#username").val() + ',' + $("#password").val(), function(data) {
						if (data != null) {
							if (data['message'] == "true") {
								if (data['type'] == "admin") {
									window.location = "PresentationLayer/admin/";
								} else {
									window.location = "PresentationLayer/beneficiary/";
								}
							} else {
								status.text("Usuario y/o contraseña incorrectos").addClass("invalid");
							}
						} else {
							status.text("Error al intentar Iniciar sesión").addClass("invalid");
						}
					});
				} else {
					status.text("Introduzca los datos de sesión").addClass("invalid");
				}
			});
			
			$("#password").keyup(function(event){
				if(event.keyCode == 13){
					$("#loginButton").click();
				}
			});
		 });
	</script>
    
    <style scoped>
		html, body, #wrapper {
		   height:100%;
		   width: 100%;
		   margin: 0;
		   padding: 0;
		   border: 0;
		}
		
		#wrapper td {
		   vertical-align: middle;
		   text-align: center;
		}
    </style>
    
</html>