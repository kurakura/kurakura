<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	    <title>Admin</title>
    </head>
    
    <body>
	    <div id="selectService" class="k-content" style="height:100%">
            <div id="serviceGrid" style="height:92%" />
            <button id="addServiceButton" class="k-button">Alta</button>
            <div style="float:right;">
           		<button id="removeServiceButton" class="k-button k-state-disabled" disabled="disabled">Baja</button>
            </div>
        </div>
          
		<script>
			var serviceDataSource;
		    var isAddingService = false;
			var isRemoving = false;
			var enabledSelectService = true;
			var selectedIdService;
			
			function refreshService() {
				serviceDataSource.read();
			}
			
			function reselectService() {
				$("#serviceGrid").data("kendoGrid").select($("tr:contains('" + selectedIdService + "')"));
			}
			
			function enableSelectService(enable) {
				enabledSelectService = enable;
				if (enable) {
					$("#selectService").removeClass('k-state-disabled');
					document.getElementById("removeServiceButton").disabled = false;
					$("#removeServiceButton").removeClass('k-state-disabled');
					document.getElementById("addServiceButton").disabled = false;
					$("#addServiceButton").removeClass('k-state-disabled');
					
					$(".cover").remove();
				} else {
					$("#selectService").addClass('k-state-disabled');
					
					document.getElementById("removeServiceButton").disabled = true;
					$("#removeServiceButton").addClass('k-state-disabled');
					document.getElementById("addServiceButton").disabled = true;
					$("#addServiceButton").addClass('k-state-disabled');
					
					var cover = $("#serviceGrid");
					var width = cover.eq(0).outerWidth();
					var height = cover.eq(0).outerHeight();
					
					cover.each( function(index){
						var position = $(this).offset();
						$("<div class='cover'>").css({
							"width": width,
							"height": height,
							"position": "absolute",
							"top": position.top,
							"left": position.left
						}).appendTo($("body")); 
					});
				}
			}
		
           $(document).ready(function() {
				serviceDataSource = new kendo.data.DataSource({
					transport: {
						read: {
							url: "../../ServiceLayer/?adminService/listAll",
							dataType: "json"
						}
					}, 
					pageSize: 15,
					schema: {
						model: {
							id: "id",
							fields: {
								id: { type: "number" },                    
								phone: { type: "string" },
								name: { type: "string" }
							}
						}
					}
				});
			   
				$("#serviceGrid").kendoGrid({
                    dataSource: serviceDataSource,
                    sortable: true,
                    pageable: true,
					filterable: true,
					selectable: "row",
					navigatable: true,
					filterable: {
						messages: {
							info: "Filtrar por:",
							filter: "Filtrar",
							clear: "Limpiar",
							 
							isTrue: "es verdadero",
							isFalse: "es false",
							 
							and: "y",
							or: "o"
						},
						operators: { 
							string: {
							   eq: "Igual a",
							   neq: "Diferente a",
							   startswith: "Empieza por",
							   contains: "Contiene"
							}
						}												
					},
                    columns: [ 
						   {
                            field: "id",
                            width: "15%",
                            title: "ID",
                        }, {
                            field: "phone",
                            width: "30%",
                            title: "Teléfono"
                        }, {
                            field: "name",
                            width: "55%",	
                            title: "Contratante"
                        }
                    ],
					change: function(e) {
						if (enabledSelectService) {
							selectedIdService = $.map(this.select(), function (item) {
								return (item.cells[0].innerHTML);                
							});
							selectService(selectedIdService);
							if (selectedIdService == '') {
								document.getElementById("removeServiceButton").disabled = true;
								$("#removeServiceButton").addClass('k-state-disabled');
							} else {
								document.getElementById("removeServiceButton").disabled = false;
								$("#removeServiceButton").removeClass('k-state-disabled');
							}	
						} 
					},
					dataBound: function() {
						if (!isAddingService && !isRemoving) {
							setTimeout(function() {
								reselectService();
							}, 100);
						} else {
							isAddingService = false;
							isRemoving = false;
						}
					}
                });

				$(window).bind("resize", function() {
					var gridElement = $("#serviceGrid"),
						newHeight = gridElement.innerHeight(),
						otherElements = gridElement.children().not(".k-grid-content"),
						otherElementsHeight = 0;
				
					otherElements.each(function(){
						otherElementsHeight += $(this).outerHeight();
					});
				
					gridElement.children(".k-grid-content").height(newHeight - otherElementsHeight);
				});	
				
				$("#addServiceButton").click(function() {
					addService();
					isAddingService = true;
					$("#serviceGrid").data("kendoGrid").clearSelection();
					document.getElementById("removeServiceButton").disabled = true;		
					$("#removeServiceButton").addClass('k-state-disabled');
				});
				
				$("#removeServiceButton").click(function() {  
					if (confirm("¿Está seguro que desea eliminar el Servicio con ID: " + selectedIdService + "?")) {
						removeService();
						$.ajax ({
							type: 'POST',
							url: '../../ServiceLayer/?adminService/delete',
							success: function(result) {
								isRemoving = true;
								refreshService();
							}
						})
					}					
				});
            });					
        </script>
	</body>
</html>