<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Admin</title>
    </head>
    
    <body>   
        <div id="residentsGrid" style="height:50%" />
        <button id="addResidentButton" class="k-button">Alta</button>
        <div style="float:right;">
            <button id="removeResidentButton" class="k-button k-state-disabled" disabled="disabled">Baja</button>
        </div>
        
        <div id="residentForm" class="form" style="border:#DDD 1px solid;">
            <div class="columns">
                <div class="column" style="border-right:#DDD 1px solid;">
                    <ul>
                        <li>
                            <label for="dniLabel" class="required">DNI</label>
                            <input type="text" id="residentDni" name="ResidentDni" class="k-textbox" placeholder="DNI" pattern="\d{8}" required validationMessage="8 dígitos" />
                        </li>
                        
                        <li>
                            <label for="name" class="required">Nombre</label>
                            <input type="text" id="residentName" name="ResidentName" class="k-textbox" placeholder="Nombre" required validationMessage="Nombre" />
                        </li>
                        
                        <li>
                            <label for="surname" class="required">Apellidos</label>
                            <input type="text" id="residentSurname" name="ResidentSurname" class="k-textbox" placeholder="Apellidos" required validationMessage="Apellidos" />
                        </li>
                        
                        <li>
                            <label for="birthday" class="required">Nacimiento</label>
                            <input id="residentBirthdate" value="01-01-1950" style="width:150px;" required validationMessage="Nacimiento"/>
                        </li>
                    </ul>
                </div>
                
                <div class="column" style="height:142px; border-right:#DDD 1px solid;">
                    <ul>
                        <li>
                           <label for="username" class="required">Usuario</label>
                           <input type="text" id="residentUsername" name="ResidentUsername" class="k-textbox" placeholder="Usuario" required validationMessage="Usuario" />
                        </li>
                        
                        <li>
                            <label for="email">Email</label>
                            <input type="email" id="residentEmail" name="ResidentEmail" class="k-textbox" placeholder="Email" />
                        </li>
                        
                        <li>
                            <label for="phone" class="required">Teléfono</label>
                            <input type="text" id="residentPhone" name="ResidentPhone" class="k-textbox" pattern="\d{9}" placeholder="Teléfono" required validationMessage="9 dígitos"/>
                        </li>
                    </ul>
                </div>
                
                <div class="column">
                    <ul>   
                        <li>
                            <label for="email">Notas médicas</label>
                        </li>
                                 
                        <li>
                            <textarea name="ResidentMedicalNotes" class="k-textbox" id="residentMedicalNotes" style="width: 200px; height: 30px;"></textarea>
                        </li>
                        <li class="accept">
                            <button id="residentSubmit" class="k-button" type="submit">Aceptar</button>
                            
                            <button id="residentCancel" class="k-button" type="submit">Cancelar</button>
                        </li>
                        
                        <li class="status">
                        </li>
                    </ul>
                </div>
            </div>
            
            <div class="clear"></div>
        </div>
        
		<script>
            var residentsDataSource;
            var selectetResidentDni;
            var isAddingResident = true;
            
            function refreshManagementResident() {
                residentsDataSource.read();
            }
            
            function clearResidentForm() {
                $("#residentDni").val('');
                $("#residentName").val('');
                $("#residentSurname").val('');
                $("#residentBirthdate").val('01-01-1950');
                $("#residentPhone").val('')
                $("#residentEmail").val('');
                $("#residentUsername").val('');
                $("#residentMedicalNotes").val('');
            }
        
            $(document).ready(function() {
                residentsDataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: "../../ServiceLayer/?resident/listAll",
                            dataType: "json"
                        }
                    }, 
                    pageSize: 15,
                    schema: {
                        model: {
                            id: "idService",
                            fields: {
                                idService: { type: "number" },                    
                                phone: { type: "string" },
                                contractorName: { type: "string" }
                            }
                        }
                    }
                });
               
                $("#residentsGrid").kendoGrid({
                    dataSource: residentsDataSource,
                    sortable: true,
                    filterable: true,
                    selectable: "row",
                    navigatable: true,
                    filterable: {
                        messages: {
                            info: "Filtrar por:",
                            filter: "Filtrar",
                            clear: "Limpiar",
                             
                            isTrue: "es verdadero",
                            isFalse: "es false",
                             
                            and: "y",
                            or: "o"
                        },
                        operators: { 
                            string: {
                               eq: "Igual a",
                               neq: "Diferente a",
                               startswith: "Empieza por",
                               contains: "Contiene"
                            }
                        }												
                    },
                    columns: [ 
                           {
                            field: "dni",
                            width: "10%",
                            title: "DNI",
                        }, {
                            field: "name",
                            width: "20%",
                            title: "Nombre"
                        }, {
                            field: "surname",
                            width: "25%",	
                            title: "Apellidos"
                        }, {
                            field: "username",
                            width: "15%",
                            title: "Username"
                        }, {
                            field: "age",
                            width: "5%",	
                            title: "Edad"
                        }, {
                            field: "email",
                            width: "20%",	
                            title: "E-mail"
                        }, {
                            field: "phone",
                            width: "10%",
                            title: "Teléfono"
                        }
                    ],
                    change: fillResidentForm,
                    dataBound: function() {
                        //if (!isAddingResident) {
                            if (residentsDataSource.data().length > 0) {
                                document.getElementById("residentSubmit").innerHTML = "Modificar";	
                                document.getElementById("residentCancel").style.display = "none";	
                                
                                var grid = $("#residentsGrid").data("kendoGrid"),
                                row = grid.tbody.find(">tr:not(.k-grouping-row)").eq(0);
                                grid.select(row);
								isAddingResident = false;
                            } else {
                                document.getElementById("residentSubmit").innerHTML = "Aceptar";	
                                document.getElementById("residentCancel").style.display = "inline";	
                                clearResidentForm();
								isAddingResident = true;
                            }
                        //}
                    }
                });
                    
                function fillResidentForm() {
                    selectetResidentDni = $.map(this.select(), function (item) {
                        return item.cells[0].innerHTML;
                    });
                    if (selectetResidentDni != '') {
                        setState(2);
                        document.getElementById("removeResidentButton").disabled = false;
                        $("#removeResidentButton").removeClass('k-state-disabled');
                        
                        document.getElementById("residentSubmit").innerHTML = "Modificar";	
                        document.getElementById("residentCancel").style.display = "none";	
						
						document.getElementById("residentDni").disabled = true;
                    	$("#residentDni").addClass('k-state-disabled');
                    
                        $.getJSON('../../ServiceLayer/?resident/read/' + selectetResidentDni, 
                            function(data) {
                                $("#residentDni").val(data['dni']);
                                $("#residentName").val(data['name']);
                                $("#residentSurname").val(data['surname']);
                                $("#residentBirthdate").val(data['birthdate']);
                                $("#residentPhone").val(data['phone'])
                                $("#residentEmail").val(data['email']);
                                $("#residentUsername").val(data['username']);
                                $("#residentMedicalNotes").val(data['medicalNotes']);
                            }
                        );
                    } else {
                        document.getElementById("removeResidentButton").disabled = true;
                        $("#removeResidentButton").addClass('k-state-disabled');					
                    }
                };
    
                $(window).bind("resize", function() {
                    var gridElement = $("#residentsGrid"),
                        newHeight = gridElement.innerHeight(),
                        otherElements = gridElement.children().not(".k-grid-content"),
                        otherElementsHeight = 0;
                
                    otherElements.each(function(){
                        otherElementsHeight += $(this).outerHeight();
                    });
                
                    gridElement.children(".k-grid-content").height(newHeight - otherElementsHeight);
                });		
                
                var validator = $("#residentForm").kendoValidator().data("kendoValidator"),
                status = $(".status");
                
                $("#addResidentButton").click(function() {
                    //if (!isAddingResident) {
                        clearResidentForm();
                        isAddingResident = true;
                        $("#residentsGrid").data("kendoGrid").clearSelection();
                        document.getElementById("residentSubmit").innerHTML = "Aceptar";	
                        document.getElementById("residentCancel").style.display = "inline";	
                        $("#residentsGrid").addClass('k-state-disabled');
						
						document.getElementById("residentDni").disabled = false;
                    	$("#residentDni").removeClass('k-state-disabled');
                        
                        var cover = $("#residentsGrid");
                        var width = cover.eq(0).outerWidth();
                        var height = cover.eq(0).outerHeight();
                        
                        cover.each( function(index){
                            var position = $(this).offset();
                            $("<div class='cover'>").css({
                                "width": width,
                                "height": height,
                                "position": "absolute",
                                "top": position.top,
                                "left": position.left
                            }).appendTo($("body")); 
                        });
                        
                        setState(1);
                    //}
                });
                
                $("#removeResidentButton").click(function() {
					if ($("#residentsGrid").data("kendoGrid").dataSource.total() > 1) {
						if (confirm("¿Está seguro que desea eliminar el Residente con DNI: " + selectetResidentDni + "?")) {
							$.ajax({
								type: 'POST',
								url: '../../ServiceLayer/?resident/delete/' + selectetResidentDni
							});
							refreshManagementResident();
						}
					} else {
						alert("No puede eliminar el último Residente del Servicio.");
					}
                });							
    
                $("#residentSubmit").click(function() {
					status.removeClass("invalid");
					status.removeClass("valid");
                    if (validator.validate()) {
						if (isAddingResident) {
							$.ajax({
								type: 'POST',
								url: '../../ServiceLayer/?resident/create/' + $("#residentDni").val() + ',' + $("#residentName").val() + ',' + $("#residentSurname").val() + ',' + $("#residentBirthdate").val() + ',' + $("#residentUsername").val() + ',' + $("#residentPhone").val() + ','  + $("#residentEmail").val()+ ',' + $("#residentMedicalNotes").val(),
								success: function(result) {
									if (result == "") {
										isAddingResident = false;
										refreshManagementResident();
										$(".cover").remove();
										$("#residentsGrid").removeClass('k-state-disabled');
										status.text("Realizado correctamente").addClass("valid");
									} else {
										status.text("Error").addClass("invalid");
										alert("Se ha producido un error interno.");
									}
								}
							});
						} else {
							$.ajax({
								type: 'POST',
								url: '../../ServiceLayer/?resident/update/' + $("#residentDni").val() + ',' + $("#residentName").val() + ',' + $("#residentSurname").val() + ',' + $("#residentBirthdate").val() + ',' + $("#residentPhone").val() + ',' + $("#residentEmail").val() + ',' + $("#residentMedicalNotes").val(),
								success: function(result) {
									if (result == "") {
										refreshManagementResident();
										status.text("Realizado correctamente").addClass("valid");
									} else {
										status.text("Error").addClass("invalid");
										alert("Se ha producido un error interno.")
									}
								}
							});
						}
                    } else {						
                        status.text("Hay datos inválidos").addClass("invalid");
                    }
                });
                
                $("#residentCancel").click(function() {
                    setState(2);
                    isAddingResident = false;
                    $("#residentsGrid").removeClass('k-state-disabled');
                    $(".cover").remove();
                    clearResidentForm();
                });
                
                $("#residentBirthdate").kendoDatePicker({
                    format: "dd-MM-yyyy"
                });
            });
        </script>
	</body>
</html>