<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	    <title>Admin</title>
    </head>

    <body>
		<div id="Management" class="k-content k-state-disabled">
            <div id="managementTabstrip">
                <ul>
                    <li id="managementServiceTab" class="k-state-active">
                        Gestión de Servicio
                    </li>
                    
                    <div id="infoWindow">
                        <p class="info">Seleccione un <strong>Servicio</strong> existente <br /> o <br />dé de <strong>Alta</strong> uno nuevo</p>
            
                    </div>
                    
                    <li id="managementResidentTab">
                        Gestión de Residente
                    </li>
                    
                    <li id="managementCarerTab">
                        Gestión de Cuidadores
                    </li>
                    
                    <li class="k-item k-state-disabled">
                        Gestión de Alarmas
                    </li>
                </ul>
			</div>           
		</div>
        
        
		<script>
			var selectedService = -1;
			var selectedTab = 0;
			var tabStrip;
			var activeState = 0;
			var infoWindow;
			var addingService = false;
			
			function setState(state) {
				activeState = state;
				switch (activeState) {
					//Init
					case 0:
						selectedService = -1;
						//TODO
						tabStrip.select(0);
						clearService();
						enableService(false);
						enableSelectService(true);
						$("#Management").addClass('k-state-disabled');
						for (i = 0; i < 3; i++) {
							var tab = tabStrip.tabGroup.children("li").eq(i);
							tabStrip.enable(tab, false);
						}
						
						infoWindow.data("kendoWindow").open();						
						break;
					//Adding
					case 1:
						enableService(true);
						enableSelectService(false);
						$("#Management").removeClass('k-state-disabled');
						for (i = 0; i < 3; i++) {
							var tab = tabStrip.tabGroup.children("li").eq(i);
	
							tabStrip.enable(tab, false);
						}            
						
						infoWindow.data("kendoWindow").close();
						break;
					//Editing	
					case 2:
						enableService(true);
						enableSelectService(true);
						$("#Management").removeClass('k-state-disabled');
						for (i = 0; i < 3; i++) {
							var tab = tabStrip.tabGroup.children("li").eq(i);
	
							tabStrip.enable(tab, true);
						}
						infoWindow.data("kendoWindow").close();
						break;
				}
			}			
			
			function addService() {
				setState(1);
				selectedService = -1;
				//TODO
				tabStrip.select(0);
				addingService = true;
				clearService();
			}
			
			function removeService() {
				setState(0);
			}
			
			function selectService(id) {
				if (id == '') {
					setState(0);
				} else {
					selectedService = id;
					setState(2);
					$.ajax ({
						type: 'POST',
						url: '../../ServiceLayer/?adminService/select/' + id
					})
					refreshTabs();
				}
			}
			
			function refreshTabs() {
				switch (selectedTab) {
					case 0:
						if (activeState != 0){
							fillService();		
						} else
							setState(0);
						break;
					case 1:
						refreshManagementResident();
						break;
						
					case 2:
						refreshManagementCarer();
						break;
				}
			}
			
			$(document).ready(function () {
				infoWindow = $("#infoWindow");
				infoWindow.kendoWindow({
					width: "550px",
					height: "180px",
					title: "",
					resizable: false,
					actions: false,
					draggable: false
				});
				
				infoWindow.data("kendoWindow").center();
							
				$("#managementTabstrip").kendoTabStrip({
					animation: { open: { effects: "fadeIn"} },
					contentUrls: [
								'ManagementService.php',
								'ManagementResident.php',																	
								'ManagementCarer.php'
								],
					activate: onActivate
				});	
				
				var tabStripElement = $("#managementTabstrip").kendoTabStrip();
				tabStrip = tabStripElement.data("kendoTabStrip");
				
				var expandContentDivs = function(divs) {
					divs.height(divs.parent(this).parent(this).parent(this).height() - tabStripElement.children(".k-tabstrip-items").outerHeight() - 14);
				}
				
				var resizeAll = function() {
					expandContentDivs(tabStripElement.children(".k-content"));
				}
				
				function onActivate(e) {
					selectedTab = tabStrip.select().index();
                   	resizeAll();
					refreshTabs();
                }
								
				$(window).resize(function(){
					resizeAll();
				});	
			});
		</script>
    </body>
</html>