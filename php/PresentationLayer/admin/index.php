<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Kura-kura Administration</title>
        <?php 
			include_once("../header.php"); 
		
			session_start(); 
			if (!isset($_SESSION['registeredUser']) || $_SESSION['registeredUser']['type'] != 'admin')
				header( 'Location: ../../' );
		?>
    </head>

    <body>
        <div id="admin" class="admin" style="height:100%;">
        	<div id="header" style="height:80px;">
            </div>
            
            <div id="splitter" class="kk-admin-content">
                <div id="servicePane">
                    
                </div>
                <div id="contentPane">
   
                </div>
            </div>
        </div>
        
         <script>
				var content = "Management.php";
				var selectService = "SelectService.php";
                $(document).ready(function() {
    
                    var splitter = $("#splitter").kendoSplitter({
                        orientation: "horizontal",
                        panes: [
                            { collapsible: true, size: "20%", contentUrl: selectService, min: "350px", max: "700px"},
                            { collapsible: false, contentUrl: content }
                        ]
                    }).data("kendoSplitter");
					
					$("#header").load("../headerContent.php");
                });
            </script>
            
			<style>
				html,
				body {
					height:100%;
					margin:0;
					padding:0;
				}
				 
				html {
					overflow:hidden;
				}				
			</style>
    </body>
</html>