<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Admin</title>
    </head>
    
    <body>   
        <!--<div id="ManagementCarer" class="k-content" style="height:100%">-->
        	<div id="carersGrid" style="height:50%" />
            <button id="addCarerButton" class="k-button">Alta</button>
            <div style="float:right;">
           		<button id="removeCarerButton" class="k-button k-state-disabled" disabled="disabled">Baja</button>
            </div>
            <div id="carerForm"></div>
        
        <script>
			var carerDataSource;
			var isAddingCarer = true;
						
			function refreshCarersList() {
				refreshManagementCarer();
				$(".cover").remove();
				$("#carersGrid").removeClass('k-state-disabled');
			}
			
			function refreshManagementCarer() {
				carerDataSource.read();
				//isAddingCarer = false;
			}
			
			function cancelAddingCarer() {
				setState(2);
				isAddingCarer = false;
				$("#carersGrid").removeClass('k-state-disabled');
				$(".cover").remove();
				clearCarerForm();
			}
			
			$(document).ready(function() {
				carerDataSource = new kendo.data.DataSource({
					transport: {
						read: {
							url: "../../ServiceLayer/?carer/listAll/1",
							dataType: "json"
						}
					}				
				});
			   
				$("#carersGrid").kendoGrid({
                    dataSource: carerDataSource,
                    sortable: true,
					filterable: true,
					selectable: "row",
					navigatable: true,
					filterable: {
						messages: {
							info: "Filtrar por:",
							filter: "Filtrar",
							clear: "Limpiar",
							 
							isTrue: "es verdadero",
							isFalse: "es false",
							 
							and: "y",
							or: "o"
						},
						operators: { 
							string: {
							   eq: "Igual a",
							   neq: "Diferente a",
							   startswith: "Empieza por",
							   contains: "Contiene"
							}
						}												
					},
                    columns: [ 
							{
								field: "dni",
								width: "10%",
								title: "DNI",
							}, {
								field: "name",
								width: "15%",
								title: "Nombre"
							}, {
								field: "surname",
								width: "20%",	
								title: "Apellidos"
							}, {
								field: "username",
								width: "15%",
								title: "Username"
							}, {
								field: "email",
								width: "15%",	
								title: "E-mail"
							}, {
								field: "phone",
								width: "10%",
								title: "Teléfono"
							}, {
								field: "isResponsible",
								width: "7%",	
								template: '<input type="radio" name="group1" #if (isResponsible) {# checked="checked" #}# disabled="disabled"/>',
								title: "Responsable",
							}
                    ],
					change: function() {
						selectetCarerDni = $.map(this.select(), function (item) {
							return item.cells[0].innerHTML;
						});
						
						/*selectetIsResponsable = $.map(this.select(), function (item) {
							console.log(item.cells[6]);
							return item.cells[6].innerHTML;
						})*/;
						if (selectetCarerDni != '') {
							setState(2);
							document.getElementById("removeCarerButton").disabled = false;
							$("#removeCarerButton").removeClass('k-state-disabled');
							
							showModify(true);
							
							var data = carerDataSource.view();
							var selected = $.map(this.select(), function(item) {
								var carerData = new Array(data[$(item).index()].dni, data[$(item).index()].name, data[$(item).index()].surname, data[$(item).index()].phone, data[$(item).index()].email, data[$(item).index()].username, data[$(item).index()].channel);
								fillCarerForm(carerData);
								return data[$(item).index()].dni;
							});
							
							if (selected >= 0) 
								isAddingCarer = false;
							else
								isAddingCarer = true;
						} else {
							document.getElementById("removeCarerButton").disabled = true;
							$("#removeCarerButton").addClass('k-state-disabled');					
						}
					},
					dataBound: function() {
						//if (!isAddingCarer) {
							if (carerDataSource.data().length > 0) {
								
								showModify(true);
								
								var grid = $("#carersGrid").data("kendoGrid"),
								row = grid.tbody.find(">tr:not(.k-grouping-row)").eq(0);
								grid.select(row);
								
								isAddingCarer = false;
							} else {
								clearCarerForm();
								
								isAddingCarer = true;
							}
						//}
					}
				});

				$(window).bind("resize", function() {
					var gridElement = $("#carersGrid"),
						newHeight = gridElement.innerHeight(),
						otherElements = gridElement.children().not(".k-grid-content"),
						otherElementsHeight = 0;
				
					otherElements.each(function(){
						otherElementsHeight += $(this).outerHeight();
					});
				
					gridElement.children(".k-grid-content").height(newHeight - otherElementsHeight);
				});	
				
				$("#addCarerButton").click(function() {
					//if (!isAddingCarer) {
						clearCarerForm();
						isAddingCarer = true;
						$("#carersGrid").data("kendoGrid").clearSelection();
						/*document.getElementById("carerSubmit").innerHTML = "Aceptar";	
						document.getElementById("carerCancel").style.display = "inline";*/
						showModify(false);
						$("#carersGrid").addClass('k-state-disabled');
						
						var cover = $("#carersGrid");
						var width = cover.eq(0).outerWidth();
						var height = cover.eq(0).outerHeight();
						
						cover.each( function(index){
							var position = $(this).offset();
							$("<div class='cover'>").css({
								"width": width,
								"height": height,
								"position": "absolute",
								"top": position.top,
								"left": position.left
							}).appendTo($("body")); 
						});
						
						setState(1);
					//}
				});
				
				$("#removeCarerButton").click(function() {
					$.ajax({
						type: 'POST',
						url: '../../ServiceLayer/?carer/couldDeleted/' + selectetCarerDni,
						success: function(e) {
							if (e == "true") {
								if (confirm("¿Está seguro que desea eliminar el Cuidador con DNI: " + selectetCarerDni + "?")) {
									$.ajax({
										type: 'POST',
										url: '../../ServiceLayer/?carer/delete/' + selectetCarerDni
									});
									refreshManagementCarer();
								}
							} else {
								alert("No se puede eliminar al Responsable del Servicio.");
							}
						}
					});
				});							
				
				$("#carerForm").load("../CarerForm.php");	
			});
        </script>
	</body>
</html>