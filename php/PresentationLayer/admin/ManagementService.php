<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Admin</title>
    </head>
    
    <body>   
        <div id="validator" class="form">
            <div class="columns">
                <div id="service" class="column" style="border-right:#DDD 1px solid;">
                    <h3>Servicio</h3>
                    <ul>
                        <li>
                            <label for="phone" class="required">Teléfono</label>
                            <input id="phone" name="Phone" type="text" class="k-textbox" pattern="\d{9}" placeholder="Télefono" required validationMessage="9 dígitos"/>               
                        </li>
                        
                        <li>
                            <label for="adress" class="required">Dirección</label>
                            <input id="adress" name="Address" type="text" class="k-textbox" placeholder="Dirección" required validationMessage="	
Dirección" />
                        </li>
                        
                        <li>
                            <label for="cp" class="required">CP</label>
                            <input id="cp" name="Cp" type="text" class="k-textbox" pattern="\d{5}" placeholder="Código postal" required validationMessage="5 dígitos" />
                        </li>
                    </ul>
                </div>
                
                <div class="column" style="border-right:#DDD 1px solid; height:158px;">
                <h3> </h3>
                    <ul>
                        <li>
                            <label for="province" class="required">Provincia</label>
                            <select id="province" name="Province" placeholder="Provincia" required validationMessage="Provincia" />
                        </li>
                        
                        <li>
                            <label for="city" class="required">Población</label>
                            <select id="city" name="City" placeholder="Población" required validationMessage="Población" />
                        </li>
                    </ul>
                </div>

                <div id="history" class="column">
                    <h3>Historial</h3>
                    <ul>
                        <li>
                          <textarea name="History" cols="50" rows="5" style="width: 300px; height: 95px;" disabled="disabled" class="k-textbox" id="history"></textarea>
                      </li>
                    </ul>
                </div>                    
            </div>
            
            <div class="clear"></div>
              
            <hr />	
            
            <div class="columns">
                <div id="contractor" class="column" style="border-right:#DDD 1px solid;">
                    <h3>Contratante</h3>
                    <ul>
                        <li>
                            <label for="dni" class="required">DNI</label>
                            <input type="text" id="dni" name="Dni" class="k-textbox" placeholder="DNI" pattern="\d{8}" required validationMessage="8 dígitos"/>
                        </li>
                        
                        <li>	
                            <label for="name" class="required">Nombre</label>
                            <input id="name" name="Name" type="text" class="k-textbox" placeholder="Nombre" required validationMessage="Nombre" />
                        </li>
                        
                        <li>
                            <label for="surname" class="required">Apellidos</label>
                            <input id="surname" name="LastName" type="text" class="k-textbox" placeholder="Apellidos" required validationMessage="Apellidos" />
                        </li>
                        
                        <li>
                            <label for="contractorPhone" class="required">Teléfono</label>
                            <input id="contractorPhone" name="ContractorPhone" type="text" class="k-textbox" pattern="\d{9}" placeholder="Télefono" required validationMessage="9 dígitos"/>  
                        </li>  
                    </ul>
                </div>
                                   
                <div id="banc" class="column">
                    <h3>Datos bancarios</h3>
                    <ul>
                        <li>
                            <label for="bankAccount" class="required">C/C</label>
                            <input type="text" id="bankAccount" name="BankAccount" class="k-textbox" placeholder="Cuenta Corriente" pattern="\d{20}" required validationMessage="20 dígitos" style="width:175px"/>               
                        </li>
                    </ul>
                </div>
            </div>
            
            <div class="clear"></div>
            
            <hr />
            
            <div id="cuote">
                <!--<h3>Cuota</h3>-->
                <div style="float:right;" class="columns">
                 	<div class="column" >
                    	<ul>   
                    		<li class="status">
	                        </li>
                        </ul>
					</div>
                       
                    <div class="column" style="height:20px">  
                    	<ul>
                            <li class="accept">
                                <button id="serviceSubmit" class="k-button" type="submit">Aceptar</button>
                           
                                <button id="serviceCancel" class="k-button" type="submit">Cancelar</button>
                            </li>
                       </ul>
                	</div>
                </div>
            </div>
        </div>
        
        <script>			
			var init = true;
			var initCities = false;
			var cityData;
					
			function enableDopdownlists(enable) {
				$("#province").data("kendoDropDownList").enable(enable);
				$("#city").data("kendoDropDownList").enable(enable);
			}
				
			function fillService() {
				clearValidation();
				document.getElementById("serviceSubmit").innerHTML = "Modificar";
				document.getElementById("serviceCancel").style.display = "none";
				$.getJSON('../../ServiceLayer/?adminService/read/', 
					function(data) {				
						$("#phone").val(data['sphone']);
						$("#adress").val(data['address']);
						
						$("#cp").val(data['cp']);
						$("#dni").val(data['dni']);
						$("#name").val(data['name']);
						$("#surname").val(data['surname']);
						$("#contractorPhone").val(data['phone']);
						$("#bankAccount").val(data['bankAccount']);
						$("#province").data("kendoDropDownList").search(data['province']);
						cityData = data['city']; 
						initCities = true;
						
						var city = $("#city").data("kendoDropDownList");
						city.dataSource.transport.options.read.url = "../../ServiceLayer/?adminService/city/" + data['province'];
							
						city.dataSource.read();
						
						enableDopdownlists(false);	
						document.getElementById("dni").disabled = true;
                		$("#dni").addClass('k-state-disabled');		
					});
				document.getElementById("history").hidden = false;				
			}
			
			function clearService() {
				document.getElementById("dni").disabled = false;
                $("#dni").removeClass('k-state-disabled');		
				clearValidation();
				$("#phone").val("");
				$("#adress").val("");
				$("#province").val("");
				$("#city").val("");
				$("#cp").val("");
				$("#dni").val("");
				$("#name").val("");
				$("#surname").val("");
				$("#contractorPhone").val("");
				$("#bankAccount").val("");
				$("#province").data("kendoDropDownList").select(0);
				var city = $("#city").data("kendoDropDownList");
				city.dataSource.transport.options.read.url = "../../ServiceLayer/?adminService/city/" + $("#province").data("kendoDropDownList").value();
							
				city.dataSource.read();
				
				document.getElementById("history").hidden = true;
				document.getElementById("serviceSubmit").innerHTML = "Aceptar";	
				document.getElementById("serviceCancel").style.display = "inline";	
				
				enableDopdownlists(true);	
				$("#adress").attr("disabled", false);
				$("#cp").attr("disabled", false);
			}
			
			function enableService(enable) {
				$("#validator").find(":input").attr("disabled", !enable);
				enableDopdownlists(enable);	
				$("#adress").attr("disabled", true);
				$("#cp").attr("disabled", true);
			}
			
			function clearValidation() {
				$("#validator").find("span.k-tooltip-validation").hide();
			}
				
			$(document).ready(function() {
				var validator = $("#validator").kendoValidator().data("kendoValidator");
				var status = $(".status");				
				document.getElementById("history").hidden = true;
				
				$("#serviceSubmit").click(function() {
					if (validator.validate()) {
						var url;
						
						if (addingService) {
							url = '../../ServiceLayer/?adminService/create/' + $("#dni").val() + ',' + $("#name").val() + ',' + $("#surname").val() + ',' + $("#contractorPhone").val() + ',' + $("#bankAccount").val() + ',' + $("#cp").val() + ',' + $("#city").val() + ',' + $("#adress").val() + ',' + $("#phone").val()
						} else {
							url = '../../ServiceLayer/?adminService/update/' + $("#dni").val() + ',' + $("#name").val() + ',' + $("#surname").val() + ',' + $("#contractorPhone").val() + ',' + $("#bankAccount").val() + ',' + $("#phone").val()
						}
						
						$.ajax({
							type: 'POST',
							url: url,
							success: function(result) {
								if (result.length < 10 ) {
									if (addingService) {
										isAddingService = false;
										selectedIdService = result;
										refreshService();
										setState(2);
										status.text("Realizado correctamente").addClass("valid");
									}
								} else {
									status.text("Error").addClass("invalid");
										alert("Se ha producido un error interno.");
								}
							}
						});			
					} else {
						status.text("Hay datos inválidos").addClass("invalid");
					}
				});
				
				$("#serviceCancel").click(function() {
					addingService = false;
					setState(0);	
				});			
								
				var citiesDataSource = new kendo.data.DataSource({
					transport: {
						read: {
							url: "../../ServiceLayer/?adminService/city/",
							dataType: "json"
						}
					}
				});		
				
				$("#city").kendoDropDownList({
					dataTextField: "name",
					dataValueField: "name",
					dataSource: citiesDataSource,
					dataBound: function(e) {
						setTimeout(function() {
							$("#city").val(cityData);
							$("#city").data("kendoDropDownList").search(cityData);
							if (init) {
								enableDopdownlists(false);	
								init = false;
							}
						}, 50);
					}
				});			
							
				$("#province").kendoDropDownList({
					dataTextField: "name",
					dataValueField: "name",
					dataSource: {
						transport: {
							read: {
								url: "../../ServiceLayer/?adminService/province/",
								dataType: "json"
							}
						}
					},
					dataBound: function(e) {
						if (!initCities) {
							var city = $("#city").data("kendoDropDownList");
							city.dataSource.transport.options.read.url = "../../ServiceLayer/?adminService/city/" + $("#province").data("kendoDropDownList").value();
							
							city.dataSource.read();
						}
					},
					change: function(e) {
						var city = $("#city").data("kendoDropDownList");
						city.dataSource.transport.options.read.url = "../../ServiceLayer/?adminService/city/" + $("#province").data("kendoDropDownList").value();
						
						city.dataSource.read();
					}
				});					
			});
        </script>

		<style scoped>
			textarea {
				resize: none;
			}
        </style>
	</body>
</html>