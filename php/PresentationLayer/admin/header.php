<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Header</title>
    </head>
    
    <body>
        <div class="columns">
            <div class="column">
                <div>
                	<h3 class="kk-title">Administación Kura-Kura</h3>
                </div>
            </div>
            
            <div class="column" style="float:right">
           	    <img class="logomini" src="../styles/logo-mini.png" width="58" height="75" /> </div>
            </div>
            
            <div class="column form" style="float:right">
            	<ul>
            		<li>
	           	    	<input id="logoutButton" value="Logout" class="k-button" type="submit">
                    </li>
                    
                    <li>
                    	<p> <?php session_start(); echo $_SESSION['registeredUser']['surname'] . ', ' . $_SESSION['registeredUser']['name'] ?> </p>
                    </li>
                    
                    <li>
                    	<p> <?php echo $_SESSION['registeredUser']['type'] ?> </p>
                    </li>
             	</ul>
            </div>
        </div>
        
        <script>
			$(document).ready(function() {
				$("#logoutButton").click(function() {
					$.getJSON('../../ServiceLayer/?access/logout/', function(data) {
							if (data['message'] == "true") {
								window.location = "../../";								
							} else {
								alert("Error al realizar logout.");
							}
						}
					);
				}); 
			});
		</script>
    </body>
</html>