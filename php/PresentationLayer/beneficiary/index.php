<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Kura-kura</title>
        <?php 
			include_once("../header.php"); 
			
			session_start(); 
			if (!isset($_SESSION['registeredUser']) || $_SESSION['registeredUser']['type'] == 'admin')
				header( 'Location: ../../' );
		?>
    </head>
    
    <body>
    	<div id="user" class="user" style="height:100%;">
        	<div id="header" style="height:80px;margin-bottom:10px;">
            </div>
            <div id="contentPane" style="height:100%;">              
            </div>
        </div>
    	  <script>
			$(document).ready(function(){
				$("#header").load("../headerContent.php");
				$("#contentPane").load("Management.php");
			});
		</script>
        
        <style>
			html,
			body {
				height:100%;
				margin:0;
				padding:0;
			}
			 
			html {
				overflow:hidden;
			}				
		</style>
    </body>   
</html>