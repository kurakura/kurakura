<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Cuidadores</title>
        <script src="../../PresentationLayer/KendoUI/js/console.js"></script>
    </head>
    
    <body>
    <div id="scheduleView" class="userView">
        <div id="content">
			<div id="carers" class="listCarers"></div>
            <!--<table>
            	<tr>
                	<td>-->
                        <div id="schedule" class="schedule" >
                            <div class="dayItem">
                                <div>
                                    <h3 class="dayTitle">Lunes</h3>
                                </div>
                                
                                <div id="daySplitter0" class = "daySplitter"></div>
                            </div>
                            
                            <div class="dayItem">
                                <div>
                                    <h3 class="dayTitle">Martes</h3>
                                </div>
                                
                                <div id="daySplitter1" class = "daySplitter"></div>
                            </div>
                            
                            <div class="dayItem">
                                <div>
                                    <h3 class="dayTitle">Miércoles</h3>
                                </div>
                                
                                <div id="daySplitter2" class = "daySplitter"></div>
                            </div>
                            
                            <div class="dayItem">
                                <div>
                                    <h3 class="dayTitle">Jueves</h3>
                                </div>
                                
                                <div id="daySplitter3" class = "daySplitter"></div>
                            </div>
                            
                            <div class="dayItem">
                                <div>
                                    <h3 class="dayTitle">Viernes</h3>
                                </div>
                                
                                <div id="daySplitter4" class = "daySplitter"></div>
                            </div>
                            
                            <div class="dayItem">
                                <div>
                                    <h3 class="dayTitle">Sábado</h3>
                                </div>
                                
                                <div id="daySplitter5" class = "daySplitter"></div>
                            </div>
                            
                            <div class="dayItem">
                                <div>
                                    <h3 class="dayTitle">Domingo</h3>
                                </div>
                                
                                <div id="daySplitter6" class = "daySplitter"></div>
                            </div>
                            
                        </div>
					<!--</td>
				</tr>
                <tr>
                    <td>-->
                        <div class="clear"></div>
                        <!--<div class="console" style="height:50px;"></div>-->
					<!--</td>
				</tr>
			</table>-->
        </div>
    </div>
    
    <script type="text/x-kendo-tmpl" id="template">
			<div class="carerItem" >
				<table>
					<tr>
						<td>
							<div class="colorCarerItem" style="background-color:${color};">
							</div>
						</td>	
						<td>	
							<div><p>${name} ${surname}</p>
							</div>
						</td>
					</tr>
				</table>
			</div>
     </script>
    
     <script>
	 
	 	
		$(document).ready(function(){
			var divTag, i, j, count = 4, selectedCarer = -1, selectedCarerColor;
			
			var carersDataSource = new kendo.data.DataSource({
				transport: {
					read: {
						url: "../../ServiceLayer/?carer/listAll/1", 
						dataType: "json",
					} 
				},
				pageSize: 9
			});
			
			$.getJSON('../../ServiceLayer/?carer/getSchedule', function(data) {
				for (i = 0; i < data.length; i++) {
					divTag = document.createElement("div");
					divTag.style.backgroundColor = data[i]['color'];
					
					var size = (parseInt(data[i]['endTime'], 10) - parseInt(data[i]['startTime'], 10))*300/24;
					divTag.setAttribute("id","fragmentDay"+i);
					divTag.setAttribute("class","scheduleDropPane");
					document.getElementById("daySplitter"+data[i]['day']).appendChild(divTag);
				}
				
				setTimeout(function (){
						$(".daySplitter").kendoSplitter({
							orientation: "vertical",
							resize: onResize
							
						});
						
						$(".scheduleDropPane").kendoDropTarget({
							dragenter: droptargetOnDragEnter,
							dragleave: droptargetOnDragLeave,
							drop: droptargetOnDrop
						});
						
				}, 50);
				
				setTimeout(function(){
					var splitterBarDivTag;
						for (i = 0; i < data.length; i++) {
							divTag = document.getElementById("fragmentDay"+i);
							
							var pos,size = (parseInt(data[i]['endTime'], 10) - parseInt(data[i]['startTime'], 10))*300/24;
							divTag.style.height = size+"px";
							pos = parseInt(data[i]['startTime'], 10)*300/24+"px";
							divTag.style.top = pos;
							var parentSplitter = document.getElementById("daySplitter"+data[i]['day']);
							var childs = parentSplitter.childNodes;
							
							$("#daySplitter" + data[i]['day']).data("kendoSplitter").min(divTag, "10px");							
							
							if (childs.length > 1) {
								for(e = 0; e < childs.length; e++) {
									if (divTag == childs.item(e)) {
																			
										if (e < childs.length - 2) {
											childs.item(e + 1).style.top = parseInt(data[i + 1]['startTime'], 10)*300/24 - 5 +"px";
										}
										break;
									}
								}
							}
							divTag.innerHTML = data[i]['startTime'] + " - " + data[i]['endTime'];	
						}
					}, 
				200);					
			});
			
			var carersListView = $("#carers").kendoListView({
				dataSource: carersDataSource,
				change: onSelectItem,
				selectable: "single",
				template: kendo.template($("#template").html())
			});
			
			carersListView.kendoDraggable({
          		filter: ".carerItem",
				dragstart: draggableOnDragStart,
				dragend: draggableOnDragEnd,
				cursorOffset: {top: -5, left: -5},
          		hint: function(row) {
					return row.clone();  //returns the same mark up as the template <li>some name</li>
				}
			});

			function onSelectItem(){
				var data = carersDataSource.view();
				var selected = $.map(this.select(), function(item) {
						return data[$(item).index()].dni;
				});
				var selectedColor = $.map(this.select(), function(item) {
						return data[$(item).index()].color;
				});
				selectedCarer = selected;
				selectedCarerColor = selectedColor;
			}
			
			function draggableOnDragStart(e) {
			}

			function draggableOnDragEnd(e) {		
			}
			
			function droptargetOnDragEnter(e) {
			}

			function droptargetOnDragLeave(e) {
			}

			function droptargetOnDrop(e) {
				setTimeout(function() {
					if (selectedCarer >= 0) {
						//$(e.dropTarget).text(selectedCarer);
						
						var size = $(e.dropTarget).height();
						var sizePar = $(e.dropTarget).parent(this).height();
						var pos = $(e.dropTarget).position().top;
						if (pos > 0){
							pos -= 2+5;
							size += 2+5;
						}
						var initTime, endTime, weekDay;
						initTime = Math.ceil(pos*24/sizePar);
						endTime = Math.ceil((pos+size)*24/sizePar); 						

						var firstPosition = $("#daySplitter0").position().left;
						pos =  $(e.dropTarget).parent(this).position().left;
						sizePar = $(e.dropTarget).width()*7;
						weekDay = Math.ceil((6 * (pos - firstPosition)/sizePar));

						$(e.dropTarget).attr("carerId",selectedCarer);
						
						$(e.dropTarget).css("background-color",selectedCarerColor);
						
						$.ajax({
							type: "POST",
							url: "../../ServiceLayer/?carer/modifyTurn/"+selectedCarer+","+initTime+","+endTime+","+weekDay+","+initTime
							});						
						selectedCarer = -1;
					} else {
					}
				}, 100);			
			}
			
			function onResize(e) {
				var initNextTime = 0, size, sizeParent;
				if (e.sender['resizing']) {
					sizeParent = e.sender['resizing']['previousPane'].parent().height();
					
					size = e.sender['resizing']['previousPane'].height();
					initNextTime = (e.sender['resizing']['previousPane'].position().top) + size;
					initNextTime = Math.ceil(initNextTime*24/sizeParent);
					setTimeout(function() {
						if (e.sender['resizing']) {
							var initTimePrev, initTimeNext, endTimePrev, endTimeNext, weekDay, firstPosition;
							
							//PreviousPane
							weekDay = Math.ceil(e.sender['resizing']['previousPane'].parent().position().left);
							initTimePrev = (e.sender['resizing']['previousPane'].position().top);
							size = e.sender['resizing']['previousPane'].height();
							if (initTimePrev > 0){
								initTimePrev -= 2+5;
								size += 2+5;
							}
							endTimePrev = (e.sender['resizing']['previousPane'].position().top) + size;
							
							initTimePrev = Math.ceil(initTimePrev*24/sizeParent);
							endTimePrev = Math.ceil(endTimePrev*24/sizeParent); 						
							
							firstPosition = $("#daySplitter0").position().left; 
							size = e.sender['resizing']['previousPane'].width()*7;
							weekDay = Math.ceil((6 * (weekDay - firstPosition)/size));
							
							if (endTimePrev > 24)
								endTimePrev = 24;

							$.ajax({
								type: "POST",
								url: "../../ServiceLayer/?carer/modifyTurn/"+"-1"+","+initTimePrev+","+endTimePrev+","+weekDay+","+initTimePrev
							});
							e.sender['resizing']['previousPane'].html(initTimePrev + " - " + endTimePrev);
							
							//NextPane
							initTimeNext = (e.sender['resizing']['nextPane'].position().top);
							size = e.sender['resizing']['nextPane'].height();
							if (initTimeNext > 0){
								initTimeNext -= 2+5;
								size += 2+5;
							}
							endTimeNext = Math.ceil(e.sender['resizing']['nextPane'].position().top) + size;
							
							initTimeNext = Math.ceil(initTimeNext*24/sizeParent);
							endTimeNext = Math.ceil(endTimeNext*24/sizeParent); 						
							
							if (initTimeNext < 1)
								initTimeNext = 1;
							
							if (endTimeNext > 24)
								endTimeNext = 24;
							
							//kendoConsole.log(initTimePrev + " - " + endTimePrev + " || " + endTimePrev + " - " + endTimeNext);
							
							e.sender['resizing']['nextPane'].html(endTimePrev + " - " + endTimeNext);
							
							$.ajax({
								type: "POST",
								url: "../../ServiceLayer/?carer/modifyTurn/"+"-1"+","+initNextTime+","+endTimeNext+","+weekDay+","+endTimePrev
							});
							
							
						}
						
					}, 200);
				}
			}
						
		});	
			
	</script>
    
    <style>
	
		.daySplitter {
			max-height:300px;
		}
		
		div.schedule div { 
			float: left;
		}

		div.dayItem div{
			clear:both;
			width:100px;
		}
		
		div.clear { 
			clear: both; 
		}
		
		.dropPaneOver {
			background-color:#0C3;
		}
		
		.daySplitter .k-splitbar {
			height: 5px;
			overflow: hidden;
			border-width: 0;
		}
		
		.scheduleDropPane {
			color: #FFF;
			text-align: center;
			font-weight: bold;
		}
		
		.dayTitle {
			text-align: center;
		}
		
		.carerItem {
			cursor:pointer;
		}
		
		.colorCarerItem {
			width:15px;
			height:15px;
			border:1px solid;
			border-radius: 5px;
		}
		
		.listCarers {
			float:left; 
			width:10%;
			margin:30px;
			padding:15px;
			border-radius: 5px;
			border:none;
		}
	</style>
    
	</body>
</html>