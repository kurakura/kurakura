<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Cuidadores</title>
    </head>

    <body>
    <div id="carersView" class="userView"> 
        <div id="listView" ></div>
        <div id ="bottom">
            <table>
                    <tr>
                    <td><div id="form"><p></p></div></td>
                    <td><div id="responsibleListView" style="float:right" ></div></td>
                </tr>
            </table>
        </div>
    </div>
    
	<script type="text/x-kendo-tmpl" id="template">
        # if ( dni >= "0" && !isResponsible) { #
            <div class="item" style="background-color:${color};">
                <div class="deleteItem" style="width:15px;height:15px;float:right;padding-right:10px;">
                        <button id="deleteCarer" class="k-button" style="background-color:${color};"onclick="deleteCarer(${dni})" >x</button>
                </div>
        # } else if (dni < "0") { #
            <div class="item" >
        # } else { #
            <div class="item" style="background-color:${color};">
        # } #
                <dl>
                        <dt><dd><h3>${name}</h3>
                        </dd>
                        </dt>
                        <dt><dd>${surname}</dd>
                        </dt>
                        <dt><dd>${email}</dd>
                        </dt>
                        <dt><dd>${phone}</dd>
                        </dt>
                </dl>
        </div>
    </script>

    <script>
        var carersDataSource;
        var isAddingCarer = true;
        
        function cancelAddingCarer() {
            clearCarerForm();
        }
        
        $(document).ready(function() {
            var count = 0;
            carersDataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                            url: "../../ServiceLayer/?carer/listAll/0",
                            dataType: "json"
                    } 
                },
                pageSize: 8,
                requestEnd: onRequestEnd
            });
           
            var responsibleDataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: "../../ServiceLayer/?carer/getResponsible",
                        dataType: "json",
                    } 
                },
                pageSize: 1
            });

            $("#listView").kendoListView({
                dataSource: carersDataSource,
                change: onChange,
                selectable: "single",
                template: kendo.template($("#template").html())
            });     
    
            $("#responsibleListView").kendoListView({
                dataSource: responsibleDataSource,
                change: onChangeResponsible,
                selectable: "single",
                template: kendo.template($("#template").html())
            });
    
            function onRequestEnd() {
                setTimeout(function() {
                carersDataSource.add({ dni: "-1", name:"Añadir", surname:"", email:"", phone:""});
                carersDataSource.sync();}, 100);
                
            }				
                            
            function onChangeResponsible() {
                var data = responsibleDataSource.view();
                var selected = $.map(this.select(), function(item) {
                    var carerData = new Array(data[$(item).index()].dni, data[$(item).index()].name, data[$(item).index()].surname, data[$(item).index()].phone, data[$(item).index()].email, data[$(item).index()].username, data[$(item).index()].channel);
                    fillCarerForm(carerData);
                    return data[$(item).index()].dni;
                });
            }
                                    
            function onChange() {
                var data = carersDataSource.view();
                var selected = $.map(this.select(), function(item) {
                    var carerData = new Array(data[$(item).index()].dni, data[$(item).index()].name, data[$(item).index()].surname, data[$(item).index()].phone, data[$(item).index()].email, data[$(item).index()].username, data[$(item).index()].channel);
                    fillCarerForm(carerData);
                    return data[$(item).index()].dni;
                });
                if (selected >= 0) 
                    isAddingCarer = false;
                else
                    isAddingCarer = true;
            }
                            
            $("#form").load("../CarerForm.php");
                            
        });
            
        function refreshCarersList() {
            carersDataSource.read();
        }
                    
        function deleteCarer(carerDni){
            if(confirm("¿Está seguro que desea eliminar el Cuidador con DNI:  " + carerDni + "?")) {
                $.ajax({
                    type: "POST",
                    url: "../../ServiceLayer/?carer/delete/" + carerDni
                });
                refreshCarersList();
            }
        }
                    
    </script>
    
      <style scoped>
            .item
            {
                float: left;
                width: 240px;
                height: 120px;
                margin: 20px 10px 0px 15px;
                padding: 5px;
                -moz-box-shadow: inset 0 0 30px rgba(0,0,0,0.15);
                -webkit-box-shadow: inset 0 0 30px rgba(0,0,0,0.15);
                box-shadow: inset 0 0 30px rgba(0,0,0,0.15);
                -webkit-border-radius: 15px;
                -moz-border-radius: 15px;
                border-radius: 5px;
                background-image: none;
                                
                cursor: pointer;
            }
            .item img
            {
                float: left;
                width: 110px;
                height: 110px;
                -webkit-border-radius: 10px;
                -moz-border-radius: 10px;
                border-radius: 10px;
            }
            .item h3
            {
                margin: 10px 0 0 0;
                padding: 10px 10px 10px 20px;
                font-size: 1em;
                float: top;
                max-width: 120px;
                text-transform: uppercase;
            }
            .k-state-selected h3
            {
                color: #fff;
                background-color: rgba(0,0,0,0.4);
                -moz-box-shadow: inset 0 0 20px rgba(0,0,0,0.2);
                -webkit-box-shadow: inset 0 0 20px rgba(0,0,0,0.2);
                box-shadow: inset 0 0 20px rgba(0,0,0,0.2);
                -moz-border-radius-topright: 5px;
                -moz-border-radius-bottomright: 5px;
                -webkit-border-top-right-radius: 5px;
                -webkit-border-bottom-right-radius: 5px;
                border-top-right-radius: 5px;
                border-bottom-right-radius: 5px;
            }
            .k-listview:after
            {
                content: ".";
                display: block;
                height: 0;
                clear: both;
                visibility: hidden;
            }
            .k-listview
            {
                border: 0;
                padding: 0 0 20px 0;
                min-width: 0;
            }
			
			.carerForm{
				float: left;
                margin: 5px;
                padding: 5px;
                -moz-box-shadow: inset 0 0 30px rgba(0,0,0,0.15);
                -webkit-box-shadow: inset 0 0 30px rgba(0,0,0,0.15);
                box-shadow: inset 0 0 30px rgba(0,0,0,0.15);
                -webkit-border-radius: 15px;
                -moz-border-radius: 15px;
                border-radius: 5px;
			}
        </style>  
    </body>
</html>