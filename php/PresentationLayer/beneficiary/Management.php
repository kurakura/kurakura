<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>

   	<body>
         <!--<div style="width:100%;">-->
         <table style="width:100%;height:100%;" align="left";>   	
            <tr>
                <td style="width:65px;vertical-align:top;padding-top:5px;">
                    <div id="columnItemsUser" class="form">
                        <ul>
                            <li><button id="homeItem" class="k-button itemUser">Inicio</button></li>
                            <li class="k-state-disabled"><button id="agendItem" class="k-button k-state-disabled itemUser">Agenda</button></li>
                            <li><button id="carersItem" class="k-button itemUser">Cuidadores</button></li>
                            <li><button id="scheduleItem"class="k-button itemUser">Turnos</button></li>
                            <li class="k-state-disabled"><button id="helpItem" class="k-button k-state-disabled itemUser">Ayuda</button></li>
                        </ul>
                    </div>
                </td>
                <td style="vertical-align:top; height: 100%;">
                    <div id="centralContentUser" class="userView" style="height: 100%; padding-left:10px;">
                        <div id="central-content" >
                        </div>
                    </div>
                </td>
            </tr>
        </table>
<!--		</div>-->        
        <script>			
            $(document).ready(function(){
				$("#central-content").load("Home.php");
				setTimeout(function(){setHeaderTitle("Inicio");},200);
				
				$('#homeItem').click(function(){
					setHeaderTitle("Inicio");
					$.ajax({
						type: "POST",
						url: "Home.php",
						success: function(a) {
								$('#central-content').html(a);
						}
					});
			    });
			   
				$('#carersItem').click(function(){
					setHeaderTitle("Cuidadores");
					$.ajax({
						type: "POST",
						url: "Carers.php",
						success: function(a) {
								$('#central-content').html(a);
						}
					});
			    });
			    
			    $('#scheduleItem').click(function(){
					setHeaderTitle("Turnos");
					$.ajax({
						type: "POST",
						url: "Schedule.php",
						success: function(a) {
								$('#central-content').html(a);
						}
					});
			    });
			});
        </script>
    
    	<style scoped>
			.itemUser {
				padding-bottom: 5px;
				padding-top: 5px;
				margin: 2px;
				font-size: medium;
				float: right;
			}
			
			.k-state-disabled a:hover{
				color:#CCC;
			}
		</style>
	</body>
</html>