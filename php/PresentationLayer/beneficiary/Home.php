<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Cuidadores</title>
    </head>
    
    <body>
        <div id="homeView" class="welcomePane">
            <h3 class="homeTitle">Bienvenido/a al prototipo Kura-kura, la tortuga que más te ayuda!</h3>
            <p class="homeContent">Desde aquí podrá consultar los contactos de los cuidadores asignados a su servicio, modificarlos, añadir nuevos o darlos de baja en la sección <strong class="kk-color">Cuidadores</strong>. <br/><br/>Modifique el horario de disponibilidad de cada cuidador en la sección <strong class="kk-color">Turnos</strong>.<br/>(Turnos está en fase de construcción y puede ocasionar algunos problemas.)<br/><br/>En próximas versiones, gozará de nuestro servicio de <strong class="kk-color">Agenda</strong>.<br/><br/></p>
            <p class="homeEnd">El equipo de <strong class="kk-color">Kura-kura</strong> les desea una buena experiencia!</p>
        </div>
        
        <style>
			.welcomePane {
				padding:20px;	
			}
			
			.homeTitle {
				font-size:large;
				
			}
			
			.homeContent {
				font-size:16px;
			}
			
			.homeEnd {
				font-size:large;
			}
		</style>
	</body>
</html>