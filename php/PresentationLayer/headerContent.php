<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Header</title>
    </head>
    
    <body>
        <div >
        	<table width="100%">
            	<tr>
                	<td width="25%" style="vertical-align:top">
                    	<div>
                            <h2 id="headerTitle" class="kk-title"> <?php 
								session_start(); 
								//echo $_SESSION['registeredUser']['type']; 
								if($_SESSION['registeredUser']['type'] == "admin")
									echo "Administración Kura-kura"; 
								//else echo "Kura-kura";?> </h2>
                        </div>
					</td>
                    <td width="70%">	
                    	<div class="column form" style="float:right">
                            <ul>
                                <li>
                                    <input id="logoutButton" value="Logout" class="k-button" type="submit">
                                </li>
                                
                                <li>
                                    <p> <?php echo $_SESSION['registeredUser']['surname'] . ', ' . $_SESSION['registeredUser']['name'] ?> </p>
                                </li>
                            </ul>
                        </div>
                    </td>
                    <td width="5%">
                    	<div style="float:right">
                            <img class="logomini" src="../styles/logo-mini.png" width="58" height="75" /> </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        
<!--        <div class="clear"/>-->
        
        <script>
			function setHeaderTitle(headerTitle) {
				$("#headerTitle").html(headerTitle);
				$("#headerTitle").addClass("carerTitleHeader");
			}
		
			$(document).ready(function() {
				$("#logoutButton").click(function() {
					$.getJSON('../../ServiceLayer/?access/logout/', function(data) {
							if (data['message'] == "true") {
								window.location = "../../";								
							} else {
								alert("Error al realizar logout.");
							}
						}
					);
				}); 
			});
		</script>
        
        <style scoped>
			.carerTitleHeader{
				padding-left:70px;
				font-size:x-large;

			}
		</style>
    </body>
</html>