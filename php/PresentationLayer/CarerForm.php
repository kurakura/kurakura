<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Cuidadores</title>
    </head>
    
    <body>
        <div id="carerForm" class="form carerForm" style="border:#DDD 1px solid;">
        	<div class="columns">
                <div class="column" style="border-right:#DDD 1px solid;">
                    <ul>
                        <li>
                            <label for="carer" class="required">DNI</label>
                            <input type="text" id="carerDni" name="CarerDni" class="k-textbox" pattern="\d{8}" placeholder="DNI" required validationMessage="8 dígitos" />
                        </li>
                        
                        <li>
                            <label for="name" class="required">Nombre</label>
                            <input type="text" id="carerName" name="CarerName" class="k-textbox" placeholder="Nombre" required validationMessage="Nombre" />
                        </li>
                        
                        <li>
                            <label for="lastName" class="required">Apellidos</label>
                            <input type="text" id="carerSurname" name="CarerSurname" class="k-textbox" placeholder="Apellidos" required validationMessage="Apellidos" />
                        </li>
                    </ul>
                </div>
                
                <div class="column" style="border-right:#DDD 1px solid;">
                    <ul>
                        <li>
                           <label for="userName" class="required">Usuario</label>
                            <input type="text" id="carerUsername" name="CarerUsername" class="k-textbox" placeholder="Usuario" required validationMessage="Usuario" />
                        </li>
                        
                        <li>
                            <label for="email" class="required">Email</label>
                            <input type="email" id="carerEmail" name="CarerEmail" class="k-textbox" placeholder="Email" required validationMessage="Email"/>
                        </li>
                        
                        <li>
                            <label for="phone" class="required">Teléfono</label>
                            <input type="text" id="carerPhone" name="CarerPhone" class="k-textbox" pattern="\d{9}" placeholder="Teléfono" required validationMessage="9 dígitos"/>
                        </li>
                    </ul>
                </div>
                
                <div class="column">
                    <ul>
                        <li>
                            <label for="channel" class="required">Medio de aviso</label>
                            <select id="carerChannel" placeholder="Seleccione un medio...">
                                <option>Email</option>
                                <option>SMS</option>
                            </select>
                        </li>
        
                        <li class="accept">                           
                            <button id="carerSubmit" class="k-button" type="submit">Aceptar</button>
                                
                            <button id="carerCancel" class="k-button" type="submit">Cancelar</button>
                        </li>
                        
                        <li class="status">
                        </li>
                    </ul>
                </div>
            </div>
            
            <div class="clear"></div>
        </div>

		<script>
		
			function clearCarerForm() {
				$("#carerDni").val("");
				$("#carerName").val("");
				$("#carerSurname").val("");
				$("#carerPhone").val("");
				$("#carerEmail").val("")
				$("#carerUsername").val("");
				$("#carerChannel").data("kendoDropDownList").select(0);
			}
			
            function fillCarerForm(carerData) {
                if(carerData[0] > 0) {
                    $("#carerDni").val(carerData[0]);
                    $("#carerName").val(carerData[1]);
                    $("#carerSurname").val(carerData[2]);
                    $("#carerPhone").val(carerData[3])
                    $("#carerEmail").val(carerData[4]);
                    $("#carerUsername").val(carerData[5]);
                    $("#carerChannel").data("kendoDropDownList").search(carerData[6]);
					showModify(true);
                } else {
                    clearCarerForm();
					showModify(false);
                }
            };
			
			function showModify(boolean) {
				if (boolean) {
					document.getElementById("carerSubmit").innerHTML = "Modificar";	
					document.getElementById("carerCancel").style.display = "none";
					document.getElementById("carerDni").disabled = true;
                    $("#carerDni").addClass('k-state-disabled');					
				} else {
					document.getElementById("carerSubmit").innerHTML = "Aceptar";	
					document.getElementById("carerCancel").style.display = "inline";
					document.getElementById("carerDni").disabled = false;
                    $("#carerDni").removeClass('k-state-disabled');	
				}
			}
            
            $(document).ready(function() {
               
                var validator = $("#carerForm").kendoValidator().data("kendoValidator"),
                status = $(".status");

                $("#carerSubmit").click(function() {
					status.removeClass("invalid");
					status.removeClass("valid");
                    if (validator.validate()) {
                        if (isAddingCarer) {
                            $.ajax({
                                type: "POST",
                                url: "../../ServiceLayer/?carer/create/false," + $('#carerChannel').val() + "," + $('#carerEmail').val() + "," + $('#carerPhone').val() + "," + $('#carerUsername').val() + "," + $('#carerSurname').val() + "," + $('#carerName').val() + "," + $('#carerDni').val(),
								success: function(result) {
									if (result == "") {
										refreshCarersList();
										status.text("Realizado correctamente").addClass("valid");
									} else {
										status.text("Error").addClass("invalid");
										alert("Se ha producido un error interno. Consulte con el administrador.");
									}
								}
                            }); 
                        } else {
                            $.ajax({
                                type: "POST",
                                url: "../../ServiceLayer/?carer/update/" + $('#carerChannel').val() + "," + $('#carerEmail').val() + "," + $('#carerPhone').val() + "," + $('#carerUsername').val() + "," + $('#carerSurname').val() + "," + $('#carerName').val() + "," + $('#carerDni').val(),
								success: function(result) {
									if (result == "") {
										refreshCarersList();
										status.text("Realizado correctamente").addClass("valid");
									} else {
										status.text("Error").addClass("invalid");
										alert("Se ha producido un error interno. Consulte con el administrador.");
									}
								}
                            });
                        }
                    } else {
                        status.text("Hay datos inválidos").addClass("invalid");
                    }
                });
				
				$("#carerCancel").click(function() {
					cancelAddingCarer();
				});
				                
                $("#carerChannel").kendoDropDownList();
            });
        </script>      
    </body>
</html>