<?php

try {
	date_default_timezone_set('Europe/Paris');
	
	include_once dirname(dirname(__DIR__)).'/libs/orm/erm.php';
	
	$em = EntityManagerFactory::createEntityManager();
	
	/************************************/
	/* Juego de pruebas 1. 4 servicios. */
	/************************************/
	
	 /************************
	* Ciudades y Provincias *
   ************************/
	
	//Provincias
	$baleares = new entities\Province("Baleares");
	$em->persist($baleares);
	$cataluna = new entities\Province("Catalunya");
	$em->persist($cataluna);
	
	//Ciudades
	$sanMiquel = new entities\City("Sant Miquel de Balanyà", "08554", $cataluna);
	$em->persist($sanMiquel);
	$ibiza = new entities\City("Ibiza", "07800", $baleares);
	$em->persist($ibiza);
	$begues = new entities\City("Begues", "08859", $cataluna);
	$em->persist($begues);
	$barcelona = new entities\City("Barcelona", "08020", $cataluna);
	$em->persist($barcelona);
	
	
	$baleares->addCity($ibiza);
	
	$cataluna->addCity($sanMiquel);
	$cataluna->addCity($begues);
	$cataluna->addCity($barcelona);
	
	$em->flush();
	
	/*************************
	 * Admin *
	*************************/
	$administrator = new entities\Admin("99999999", "Joan", "Roc", "j@admin.com", 972711671, "admin", "admin");
	$em->persist($administrator);
	
	$em->flush();
	
	  /*************************
	 * Servicio 1 - Los Lopez *
	*************************/
	//Servicio
	$lopez = new entities\Service(false);
	$em->persist($lopez);
	
	//Contratador y Contrato
	$ctr_lopez = new entities\Contractor("11111111", "Rafael", "Lopez", "r@lopez.com", 971717171, "rlopez", "r001lpz", "12345678900987654321");
	$em->persist($ctr_lopez);
	$lopez_contract = new entities\Contract();
	$lopez_contract->setService($lopez);
	$lopez_contract->setContractor($ctr_lopez);
	$em->persist($lopez_contract);
	
	//Direccion
	$adr_lopez = new entities\Address("Joan Xico 22", "971199739", $ibiza, $lopez);
	$em->persist($adr_lopez);
	$ibiza->addAddress($adr_lopez);
	
	//Residentes
	$res1_lopez = new entities\Resident("11111112", "Josefa", "Lopez", "jlopez@aaa.com", 334226287, "jlopez", "l002lpz", "Le gusta escaparse de casa", new \DateTime("05-03-1960"), $lopez);
	$em->persist($res1_lopez);
	$lopez->addResident($res1_lopez);
	
	//Cuidadores
	$car1_lopez = new entities\Carer("11111121", "Pedro", "Gomila", "gomila@p.com", 725432682, "pgomila", "gomila", entities\Channel::SMS, true, $lopez);
	$em->persist($car1_lopez);
	$lopez->addCarer($car1_lopez);
	$car2_lopez = new entities\Carer("11111122", "Antonio", "Suarez", "asg@gmail.com", 971717171, "asuarez", "mistymountainscold", entities\Channel::Email, false, $lopez);
	$em->persist($car2_lopez);
	$lopez->addCarer($car2_lopez);
	$car3_lopez = new entities\Carer("11111123", "Maria", "Ribas", "mribas@v.com", 173523267, "mribas", "vicens", entities\Channel::Email, false, $lopez);
	$em->persist($car3_lopez);
	$lopez->addCarer($car3_lopez);
	
	//Turnos
	$turn_mon_lopez = new entities\Turn($car1_lopez, 0, 0, 12);
	$em->persist($turn_mon_lopez);
	$turn_mon_lopez2 = new entities\Turn($car2_lopez, 0, 12, 24);
	$em->persist($turn_mon_lopez2);
	$turn_tue_lopez = new entities\Turn($car1_lopez, 1, 0, 8);
	$em->persist($turn_tue_lopez);
	$turn_tue_lopez2 = new entities\Turn($car2_lopez, 1, 8, 18);
	$em->persist($turn_tue_lopez2);
	$turn_tue_lopez3 = new entities\Turn($car3_lopez, 1, 18, 24);
	$em->persist($turn_tue_lopez3);
	$turn_wed_lopez = new entities\Turn($car2_lopez, 2, 0, 24);
	$em->persist($turn_wed_lopez);
	$turn_thu_lopez = new entities\Turn($car1_lopez, 3, 0, 19);
	$em->persist($turn_thu_lopez);
	$turn_thu_lopez2 = new entities\Turn($car2_lopez, 3, 19, 24);
	$em->persist($turn_thu_lopez2);
	$turn_fri_lopez = new entities\Turn($car3_lopez, 4, 0, 24);
	$em->persist($turn_fri_lopez);
	$turn_sat_lopez = new entities\Turn($car3_lopez, 5, 0, 14);
	$em->persist($turn_sat_lopez);
	$turn_sat_lopez2 = new entities\Turn($car1_lopez, 5, 14, 24);
	$em->persist($turn_sat_lopez2);
	$turn_sun_lopez = new entities\Turn($car3_lopez, 6, 0, 10);
	$em->persist($turn_sun_lopez);
	$turn_sun_lopez2 = new entities\Turn($car1_lopez, 6, 10, 24);
	$em->persist($turn_sun_lopez2);
	
	$em->flush();
	
	
	  /**************************
	 * Servicio 2 - Los Garcia *
	*************************/
	//Servicio
	$garcia = new entities\Service(true);
	$em->persist($garcia);
	
	//Contratador y Contrato
	$ctr_garcia = new entities\Contractor("22222211", "Jose","Garcia", "j@garcia.com", 724316734, "jgarcia", "j101grc", "09876543211234567890");
	$em->persist($ctr_garcia);
	$garcia_contract = new entities\Contract();
	$garcia_contract->setService($garcia);
	$garcia_contract->setContractor($ctr_garcia);
	$em->persist($garcia_contract);
	
	//Direccion
	$adr_garcia = new entities\Address("Grande 12", "933543541", $begues, $garcia);
	$em->persist($adr_garcia);
	$begues->addAddress($adr_garcia);
	
	//Residentes
	$res1_garcia = new entities\Resident("22222212", "Manuel", "Garcia", "m@garcia.com",886246262, "mgarcia", "m102grc", null, new \DateTime("05-03-1945"), $garcia);
	$em->persist($res1_garcia);
	$garcia->addResident($res1_garcia);
	$res2_garcia = new entities\Resident("22222213", "Pepa", "Garcia", "p@garcia.com", 623584585, "pgarcia", "p103grc", "Le molestan los pies", new \DateTime("05-04-1945"), $garcia);
	$em->persist($res2_garcia);
	$garcia->addResident($res2_garcia);
	
	//Cuidadores
	$car1_garcia = new entities\Carer("22222221", "Juan", "Garcia", "j@garcia.com", 363727234, "jugarcia", "jgrc", entities\Channel::SMS, true, $garcia);
	$em->persist($car1_garcia);
	$garcia->addCarer($car1_garcia);
	$car2_garcia = new entities\Carer("22222222", "Mary", "Christmas", "ho@hoho.com", 426245375, "mchristmas", "m112sos", entities\Channel::Email, false, $garcia);
	$em->persist($car2_garcia);
	$garcia->addCarer($car2_garcia);
	
	//Turnos
	$turn_mon_garcia = new entities\Turn($car1_garcia, 0, 0, 12);
	$em->persist($turn_mon_garcia);
	$turn_mon_garcia2 = new entities\Turn($car2_garcia, 0, 12, 24);
	$em->persist($turn_mon_garcia2);
	$turn_tue_garcia = new entities\Turn($car1_garcia, 1, 0, 12);
	$em->persist($turn_tue_garcia);
	$turn_tue_garcia2 = new entities\Turn($car2_garcia, 1, 12, 24);
	$em->persist($turn_tue_garcia2);
	$turn_wed_garcia = new entities\Turn($car1_garcia, 2, 0, 12);
	$em->persist($turn_wed_garcia);
	$turn_wed_garcia2 = new entities\Turn($car2_garcia, 2, 12, 24);
	$em->persist($turn_wed_garcia2);
	$turn_thu_garcia = new entities\Turn($car1_garcia, 3, 0, 12);
	$em->persist($turn_thu_garcia);
	$turn_thu_garcia2 = new entities\Turn($car2_garcia, 3, 0, 12);
	$em->persist($turn_thu_garcia2);
	$turn_thu_garcia3 = new entities\Turn($car1_garcia, 3, 12, 24);
	$em->persist($turn_thu_garcia3);
	$turn_fri_garcia = new entities\Turn($car1_garcia, 4, 0, 24);
	$em->persist($turn_fri_garcia);
	$turn_sat_garcia = new entities\Turn($car2_garcia, 5, 0, 24);
	$em->persist($turn_sat_garcia);
	$turn_sun_garcia = new entities\Turn($car2_garcia, 6, 0, 24);
	$em->persist($turn_sun_garcia);
	
	$em->flush();
	
	  /*************************
	 * Servicio 3 - Los Perez *
	*************************/
	//Servicio
	$perez = new entities\Service(false);
	$em->persist($perez);
	
	//Contratador y Contrato
	$ctr_perez = new entities\Contractor("33333331", "Ruben","Perez", "p@perez.com",547246575, "pperez", "p201prz", "54321678905432167890");
	$em->persist($ctr_perez);
	$perez_contract = new entities\Contract();
	$perez_contract->setService($perez);
	$perez_contract->setContractor($ctr_perez);
	$em->persist($perez_contract);
	
	//Direccion
	$adr_perez = new entities\Address("Diagonal 165", "933333133", $barcelona, $perez);
	$em->persist($adr_perez);
	$barcelona->addAddress($adr_perez);
	
	//Residentes
	$res1_perez = new entities\Resident("33333312", "Maria", "Perez", "m@perez.com", 234746533, "mperez", "g3t0nmyh0rs3", null, new \DateTime("05-03-1935"), $perez);
	$em->persist($res1_perez);
	$perez->addResident($res1_perez);
	$res2_perez = new entities\Resident("33333313", "Ana", "Perez", "a@perez.com", 634134234, "aperez", "sw33tl3m0n4d3", "Es diabetica", new \DateTime("06-03-1935"), $perez);
	$em->persist($res2_perez);
	$perez->addResident($res2_perez);
	
	//Cuidadores
	$car1_perez = new entities\Carer("33333321", "Carolina", "Herrera", "ch@ny.com", 968217134, "cherrera", "newyork", entities\Channel::SMS, true, $perez);
	$em->persist($car1_perez);
	$perez->addCarer($car1_perez);
	$car2_perez = new entities\Carer("33333322", "Thorin", "Oakenshield", "thorin@erebor.com", 562717171, "thorin", "thrain", entities\Channel::Email, false, $perez);
	$em->persist($car2_perez);
	$perez->addCarer($car2_perez);
	$car3_perez = new entities\Carer("33333323", "Perico", "De Los Palotes", "perico@palotes.com", 972317561, "perico", "palotes", entities\Channel::SMS, false, $perez);
	$em->persist($car3_perez);
	$perez->addCarer($car3_perez);
	$car4_perez = new entities\Carer("33333324", "Juaquín", "De Los Palotes", "juaquin@palotes.com", 374513465, "juaquin", "palotes", entities\Channel::Email, false, $perez);
	$em->persist($car4_perez);
	$perez->addCarer($car4_perez);
	$car5_perez = new entities\Carer("33333325", "Mar", "Brava", "marbrava@marea.com", 423412876, "mar", "marbrv", entities\Channel::Email, false, $perez);
	$em->persist($car5_perez);
	$perez->addCarer($car5_perez);
	
	//Turnos
	$turn_mon_perez = new entities\Turn($car1_perez, 0, 0, 8);
	$em->persist($turn_mon_perez);
	$turn_mon_perez2 = new entities\Turn($car3_perez, 0, 8, 15);
	$em->persist($turn_mon_perez2);
	$turn_mon_perez3 = new entities\Turn($car4_perez, 0, 15, 20);
	$em->persist($turn_mon_perez3);
	$turn_mon_perez4 = new entities\Turn($car1_perez, 0, 20, 24);
	$em->persist($turn_mon_perez4);	
	$turn_tue_perez = new entities\Turn($car1_perez, 1, 0, 6);
	$em->persist($turn_tue_perez);
	$turn_tue_perez2 = new entities\Turn($car3_perez, 1, 6, 12);
	$em->persist($turn_tue_perez2);
	$turn_tue_perez3 = new entities\Turn($car2_perez, 1, 12, 24);
	$em->persist($turn_tue_perez3);	
	$turn_wed_perez = new entities\Turn($car2_perez, 2, 0, 8);
	$em->persist($turn_wed_perez);
	$turn_wed_perez2 = new entities\Turn($car4_perez, 2, 8, 24);
	$em->persist($turn_wed_perez2); 
	$turn_thu_perez = new entities\Turn($car1_perez, 3, 0, 12);
	$em->persist($turn_thu_perez);
	$turn_thu_perez2 = new entities\Turn($car2_perez, 3, 12, 24);
	$em->persist($turn_thu_perez2);
	$turn_fri_perez = new entities\Turn($car2_perez, 4, 0, 12);
	$em->persist($turn_fri_perez);
	$turn_fri_perez1 = new entities\Turn($car1_perez, 4, 12, 15);
	$em->persist($turn_fri_perez1);
	$turn_fri_perez2 = new entities\Turn($car4_perez, 4, 15, 19);
	$em->persist($turn_fri_perez2);
	$turn_fri_perez3 = new entities\Turn($car3_perez, 4, 19, 24);
	$em->persist($turn_fri_perez3);
	$turn_sat_perez = new entities\Turn($car2_perez, 5, 0, 24);
	$em->persist($turn_sat_perez);
	$turn_sun_perez = new entities\Turn($car2_perez, 6, 0, 24);
	$em->persist($turn_sun_perez);
	
	$em->flush();
	
	  /*****************************
	 * Servicio 4 - Los Fernandez *
	*****************************/
	//Servicio
	$fernandez = new entities\Service(true);
	$em->persist($fernandez);
	
	//Contratador y Contrato
	$ctr_fernandez = new entities\Contractor("44444411", "Laura", "Fernandez", "l@fernandez.com", 971717171, "lfernandez", "l301fdz", "65784930214563728190");
	$em->persist($ctr_fernandez);
	$fernandez_contract = new entities\Contract();
	$fernandez_contract->setService($fernandez);
	$fernandez_contract->setContractor($ctr_fernandez);
	$em->persist($fernandez_contract);
	
	//Direccion
	$adr_fernandez = new entities\Address("Aragon 45", "933322222", $barcelona, $fernandez);
	$em->persist($adr_fernandez);
	$barcelona->addAddress($adr_fernandez);
	
	//Residentes
	$res1_fernandez = new entities\Resident("44444412", "Jose", "Fernandez", "j@fernandez.com",234764323, "jfernandez", "j302fdz", null, new \DateTime("05-03-1935"), $fernandez);
	$em->persist($res1_fernandez);
	$fernandez->addResident($res1_fernandez);
	
	//Cuidadores
	$car1_fernandez = new entities\Carer("44444421", "Elena", "Fernandez", "e@gernandez", 634765567,"efernandez", "elefer", entities\Channel::Email, true, $fernandez);
	$em->persist($car1_fernandez);
	$fernandez->addCarer($car1_fernandez);
	
	//Turnos
	$turn_mon_fernandez = new entities\Turn($car1_fernandez, 0, 0, 24);
	$em->persist($turn_mon_fernandez);
	$turn_tue_fernandez = new entities\Turn($car1_fernandez, 1, 0, 24);
	$em->persist($turn_tue_fernandez);
	$turn_wed_fernandez = new entities\Turn($car1_fernandez, 2, 0, 24);
	$em->persist($turn_wed_fernandez);
	$turn_thu_fernandez = new entities\Turn($car1_fernandez, 3, 0, 24);
	$em->persist($turn_thu_fernandez);
	$turn_fri_fernandez = new entities\Turn($car1_fernandez, 4, 0, 24);
	$em->persist($turn_fri_fernandez);
	$turn_sat_fernandez = new entities\Turn($car1_fernandez, 5, 0, 24);
	$em->persist($turn_sat_fernandez);
	$turn_sun_fernandez = new entities\Turn($car1_fernandez, 6, 0, 24);
	$em->persist($turn_sun_fernandez);
	
	$em->flush();
	
	  /*****************************
	 * Servicio 5 - Equipo PES*
	*****************************/
	//Servicio
	$pes = new entities\Service(true);
	$em->persist($pes);
	
	//Contratador y Contrato
	$ctr_pes = new entities\Contractor("12345678", "Marc", "Mauri", "marc@pes.com", 123456789, "mauri", "residente", "54321098765432109876");
	$em->persist($ctr_pes);
	$pes_contract = new entities\Contract();
	$pes_contract->setService($pes);
	$pes_contract->setContractor($ctr_pes);
	$em->persist($pes_contract);
	
	//Direccion
	$adr_pes = new entities\Address("Piruleta 123", "112233440", $sanMiquel, $pes);
	$em->persist($adr_pes);
	$sanMiquel->addAddress($adr_pes);
	
	//Residentes
	$res1_pes = new entities\Resident("87654321", "Mayte", "Pavon", "mayte@pes.com",987654321, "mayte", "residente", null, new \DateTime("05-03-1979"), $pes);
	$em->persist($res1_pes);
	$pes->addResident($res1_pes);
	
	//Cuidadores
	$car1_pes = new entities\Carer("54321987", "Antonio", "Suarez", "antonio@pes", 666166611,"toni", "residente", entities\Channel::Email, true, $pes);
	$em->persist($car1_pes);
	$pes->addCarer($car1_pes);
	$car2_pes = new entities\Carer("24688642", "Lorena", "Bes", "lorena@pes", 226622622,"lorena", "residente", entities\Channel::Email, false, $pes);
	$em->persist($car2_pes);
	$pes->addCarer($car2_pes);
	$car3_pes = new entities\Carer("13577531", "Christian", "Martinez", "khristian@pes", 553553535,"boni", "residente", entities\Channel::Email, false, $pes);
	$em->persist($car3_pes);
	$pes->addCarer($car3_pes);
	
	//Turnos
	$turn_mon_pes = new entities\Turn($car1_pes, 0, 0, 12);
	$em->persist($turn_mon_pes);
	$turn_mon_pes2 = new entities\Turn($car2_pes, 0, 12, 18);
	$em->persist($turn_mon_pes2);
	$turn_mon_pes3 = new entities\Turn($car3_pes, 0, 18, 24);
	$em->persist($turn_mon_pes3);
	$turn_tue_pes = new entities\Turn($car2_pes, 1, 0, 10);
	$em->persist($turn_tue_pes);
	$turn_tue_pes2 = new entities\Turn($car3_pes, 1, 10, 14);
	$em->persist($turn_tue_pes2);
	$turn_tue_pes3 = new entities\Turn($car1_pes, 1, 14, 24);
	$em->persist($turn_tue_pes3);
	$turn_wed_pes = new entities\Turn($car1_pes, 2, 0, 11);
	$em->persist($turn_wed_pes);
	$turn_wed_pes2 = new entities\Turn($car3_pes, 2, 11, 14);
	$em->persist($turn_wed_pes2);
	$turn_wed_pes3 = new entities\Turn($car2_pes, 2, 14, 24);
	$em->persist($turn_wed_pes3);
	$turn_thu_pes = new entities\Turn($car3_pes, 3, 0, 14);
	$em->persist($turn_thu_pes);
	$turn_thu_pes2 = new entities\Turn($car2_pes, 3, 14, 24);
	$em->persist($turn_thu_pes2);
	$turn_fri_pes = new entities\Turn($car2_pes, 4, 0, 8);
	$em->persist($turn_fri_pes);
	$turn_fri_pes2 = new entities\Turn($car1_pes, 4, 8, 18);
	$em->persist($turn_fri_pes2);
	$turn_fri_pes3 = new entities\Turn($car3_pes, 4, 18, 24);
	$em->persist($turn_fri_pes3);
	$turn_sat_pes = new entities\Turn($car1_pes, 5, 0, 24);
	$em->persist($turn_sat_pes);
	$turn_sun_pes = new entities\Turn($car1_pes, 6, 0, 24);
	$em->persist($turn_sun_pes);
	
	$em->flush();
	
	echo "Datos cargados correctamente";
}
catch (Exception $exc) {
	echo $exc->getMessage();
}

?>