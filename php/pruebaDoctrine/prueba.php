
<?php
try {
	date_default_timezone_set('Europe/Paris');
	
	echo "PHP version ".phpversion()."\n";
	
	include_once dirname(dirname(__DIR__)).'/libs/orm/erm.php';
	
	$em = EntityManagerFactory::createEntityManager();
	
	/*$service = new entities\Service(false);
	$em->persist($service);
	
	$persona = new entities\Resident(1, "Pepa", "Jimenez", "pepa@gmail.com", 999, "pepalagrande", "pato", "medicalnote", new \DateTime("05-03-1960"), $service);
	$em->persist($persona);
	
	$persona2 = new entities\Carer(2, "Lolo", "Jimenez", "lolo@gmail.com", 999, "lologi", "pato", "Email", true, $service);
	$em->persist($persona2);
	
	$contractor = new entities\Contractor(3, "Tio", "Gilito", "pasta@gmail.com", 999, "pato", "pato", "AA01010AAA");
	$em->persist($contractor);
	
	$province = new entities\Province("Baleares");
	$em->persist($province);
	
	$city = new entities\City("Ibiza","07800",$province);
	$province->addCity($city);
	$em->persist($city);
	$city2 = new entities\City("San Rafael", "07802", $province);
	$province->addCity($city2);
	$em->persist($city2);
	
	$address = new entities\Address("Pedro Frances", 971444444, $city, $service);
	$em->persist($address);
	
	$contract = new entities\Contract();
	$contract->setService($service);
	$contract->setContractor($contractor);
	$em->persist($contract);*/
	
	$em->flush();
	
	echo "OK<br>";
	
	/*$queryString = "SELECT u.id, cc.name, cc.surname, a.phone, ci.name as city, p.name as province
	FROM entities\Service u 
	LEFT JOIN entities\Contract c WITH u = c.service 
	LEFT JOIN entities\Contractor cc WITH c.contractor = cc
	LEFT JOIN entities\Address a WHERE a.service = u
	LEFT JOIN a.city ci
	LEFT JOIN ci.province p
	WHERE u.id = 1
	";*/
	$queryString = 'SELECT u from entities\Carer u WHERE u.dni=?1';
	$query = $em->createquery($queryString);
	$query->setParameter(1,"013");
	$res = $query->getResult();
	
	echo $res[0]->getDni()." ".$res[0]->getName();
	
	$em->remove($res[0]); echo "<br>";
	$em->flush();
	
	echo $res[0]->getDni()." ".$res[0]->getName();
	
	echo "<br>";
	
	}
	catch (Exception $exc) {
		echo $exc->getMessage();
	}	
?>