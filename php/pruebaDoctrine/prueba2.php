
<?php
try {
	date_default_timezone_set('Europe/Paris');
	
	echo "PHP version ".phpversion()."\n";
	
	include_once dirname(dirname(__DIR__)).'/libs/orm/erm.php';
	
	$em = EntityManagerFactory::createEntityManager();
	
	$service = new entities\Service(false);
	$em->persist($service);
	
	$persona = new entities\Resident(1, "Pepa", "Jimenez", "pepa@gmail.com", 999, "pepalagrande", "pato", "medicalnote", new \DateTime("05-03-1960"), $service);
	$em->persist($persona);
	
	$persona2 = new entities\Carer(2, "Lolo", "Jimenez", "lolo@gmail.com", 999, "lologi", "pato", "Email", true, $service);
	$em->persist($persona2);
	
	$contractor = new entities\Contractor(3, "Tio", "Gilito", "pasta@gmail.com", 999, "pato", "pato", "AA01010AAA");
	$em->persist($contractor);
	
	$province = new entities\Province("Baleares");
	$em->persist($province);
	
	$city = new entities\City("Ibiza","07800",$province);
	$province->addCity($city);
	$em->persist($city);
	
	$address = new entities\Address("Pedro Frances", 971444444, $city, $service);
	$em->persist($address);
	
	$contract = new entities\Contract();
	$contract->setService($service);
	$contract->setContractor($contractor);
	$em->persist($contract);
	
	$em->flush();
	
	echo "OK<br>";
	
	/*$query = $em->createquery("SELECT u FROM entities\Service u");
	$res = $query->getResult();
	
	print_r($res);
	
	$em->remove($res[0]);
	$em->flush();
	
	echo "OK2<br>";
	
	$query = $em->createquery("SELECT u FROM entities\Service u");
	$res = $query->getResult();
	
	print_r($res);*/
	
	
	}
	catch (Exception $exc) {
		echo $exc->getMessage();
	}	
?>