<?php
	session_start(); 
	date_default_timezone_set('Europe/Berlin');
	
	//$_SESSION['debug'] = false;

	$query = $_SERVER['QUERY_STRING'];
	
	$request = explode('/', $query);
	
	// TODO Session
	//$rol = (!empty($request[0])) ? $request[0] : 'user';
	
	$actionController = ucfirst((!empty($request[0])) ? $request[0] : 'access') . 'Action';
	
	$action = (!empty($request[1])) ? $request[1] : 'login';
	
	$parameters = (!empty($request[2])) ? $request[2] : null ;
	
	if ($parameters != null) {
		$parameters = str_replace('%20', ' ', $parameters);
	}
	
	/*if ($_SESSION['debug']) {
		echo '<strong>[Service] FrontController</strong><br>';
		
		//<strong>Rol: </strong>' . $rol . '<br>
		echo '<strong>Query: </strong>' . $query . '<br>
			  <strong>ActionController: </strong>' . $actionController . '<br>
			  <strong>Action: </strong>' . $action . '<br>
			  <strong>Parameters: </strong>' . $parameters . '<br><br>';
	}*/
	 
	if(!@include('action/' . $actionController . '.php'))
		echo '<strong><p style="color: red">Error: ActionController, action refused</p></strong><br><br>';
	
	// Call defined function as $action
	$actionControllerInstance = new $actionController();
	if (is_callable(array($actionControllerInstance, $action))) {
		$values = explode(',', $parameters);
		call_user_func_array(array($actionControllerInstance, $action), $values);
	} else
		echo '<strong><p style="color: red">Error: Action refused</p></strong><br><br>';
?>