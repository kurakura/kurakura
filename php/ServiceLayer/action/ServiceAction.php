<?php
	//As CRUD
	include_once '../DomainLayer/Controllers/ServiceManagement.php';
	
	class ServiceAction {
					
		private $serviceManagement;
		
		public function __construct() {				
			$this->serviceManagement = new ServiceManagement();
		}
	
		public function select($id) {
			$_SESSION['selectedService'] = $id;
		}
		
		public function read() {
			$this->serviceManagement->getSelectedServiceData();
		}	
	}
?>