<?php
	//As CRUD
	include_once '../DomainLayer/Controllers/AdminServiceManagement.php';
	
	class AdminServiceAction {
					
		private $adminServiceManagement;
		
		public function __construct() {				
			$this->adminServiceManagement = new AdminServiceManagement();
		}
		
		public function create($dni, $name, $surname, $contractorPhone, $bankAccount, $CP, $city, $address, $phone) {
			$this->adminServiceManagement->addService($dni, $name, $surname, $contractorPhone, $bankAccount, $CP, $city, $address, $phone);
		}
			
		public function select($id) {
			$_SESSION['selectedService'] = $id;
		}
		
		public function listAll() {	
			$this->adminServiceManagement->listServices();
		}
		
		public function province() {
			$this->adminServiceManagement->listProvinces();
		}
		
		public function city($province) {
			$this->adminServiceManagement->listCities($province);
		}
				
		public function read() {
			$this->adminServiceManagement->getSelectedServiceData();
		}	
		
		public function update($dni, $name, $surname, $contractorPhone, $bankAccount, $phone) {			
			$this->adminServiceManagement->modifySelectedService($dni, $name, $surname, $contractorPhone, $bankAccount, $phone);
		}
		
		public function delete() {
			$this->adminServiceManagement->deleteSelectedService();
		}
	}
?>