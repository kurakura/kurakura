<?php
	//As CRUD
	include ('../DomainLayer/Controllers/ResidentManagement.php');
	
	class ResidentAction {
		private $title;
		private $tabulator;
				
		private $residentManagement;
		
		public function __construct() {		
			$this->title = '<strong>[ActionController Resident]</strong><br>';
			$this->tabulator = '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp';
		
			$this->residentManagement = new ResidentManagement();
		}
		
		public function create($dni, $name, $surname, $birthdate, $username, $phone, $email, $medicalNotes) {
			if ($_SESSION['debug']) {
				echo $this->title . 
					'<strong>create</strong> {<br>' .
					$this->tabulator . 'DNI: ' . $dni . '<br>' .
					$this->tabulator . 'Name: ' . $name . '<br>' .
					$this->tabulator . 'Last name: ' . $surname . '<br>' .
					$this->tabulator . 'Birthdate: ' . $birthdate . '<br>' .
					$this->tabulator . 'username: ' . $username . '<br>' .
					$this->tabulator . 'Phone: ' . $phone . '<br>' .
					$this->tabulator . 'E-mail: ' . $email . '<br>' .
					$this->tabulator . 'Medical notes: ' . $medicalNotes . '<br>
				}<br><br>';
			}
			
			$this->residentManagement->addResident($dni, $name, $surname, $birthdate, $username, $phone, $email, $medicalNotes);
		}
		
		public function listAll() {
			if ($_SESSION['debug']) {
				echo $this->title . 
					'<strong>listAll</strong><br><br>';
			}
				
			$this->residentManagement->listResidents();
		}
		
		public function read($dni) {
			if ($_SESSION['debug']) {
				echo $this->title . 
					'<strong>read</strong> DNI: ' . $dni . '<br><br>';
			}
			
			$this->residentManagement->getResident($dni);
		}
		
		public function update($dni, $name, $surname, $birthdate, $phone, $email, $medicalNotes) {
			if ($_SESSION['debug']) {
				echo $this->title . 
					'<strong>update</strong> {<br>' .
					$this->tabulator . 'DNI: ' . $dni . '<br>' .
					$this->tabulator . 'Name: ' . $name . '<br>' .
					$this->tabulator . 'Last name: ' . $surname . '<br>' .
					$this->tabulator . 'Birthdate: ' . $birthdate . '<br>' .
					$this->tabulator . 'Phone: ' . $phone . '<br>' .
					$this->tabulator . 'E-mail: ' . $email . '<br>' .
					$this->tabulator . 'Medical notes: ' . $medicalNotes . '<br>
				}<br><br>';
			}
			
			$this->residentManagement->modifiyResident($dni, $name, $surname, $birthdate, $phone, $email, $medicalNotes);
		}
		
		public function delete($dni) {
			if ($_SESSION['debug']) {
				echo $this->title . 
					'<strong>delete</strong> DNI: ' . $dni . '<br><br>';
			}
			
			$this->residentManagement->deleteResident($dni);
		}
	}
?>