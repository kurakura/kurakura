<?php
	//As CRUD
	include ('../DomainLayer/Controllers/CarerManagement.php');
		
	class CarerAction {
		
		private $carerManagement;
		
		public function __construct() {
			$this->carerManagement = new CarerManagement();
		}
		
		public function create($isResponsible, $channel, $email, $phone, $username, $surname, $name, $dni) {
			if ($_SESSION['debug']) {
				echo "Create a carer: DNI:" . $dni . "<br>";
			}
			
			$this->carerManagement->addCarer($isResponsible, $channel, $email, $phone, $username, $surname, $name, $dni);
		}
		
		public function listAll($all) {
			if ($_SESSION['debug']) {
				echo 'ListAll';
			}
				
			$this->carerManagement->listCarers($all);
		}
		
		public function getResponsible() {
			if ($_SESSION['debug']) {
				echo 'ListAll';
			}
				
			$this->carerManagement->getResponsible();
		}
		
		public function read($dni) {
			if ($_SESSION['debug']) {
				echo "Read a carer: DNI:" . $dni . "<br>";
			}
		}
		
		public function update($channel, $email, $phone, $username, $surname, $name, $dni) {
			if ($_SESSION['debug']) {
				echo "Update a carer: DNI:" . $dni . "<br>";
			}
			
			$this->carerManagement->modifyCarer($channel, $email, $phone, $username, $surname, $name, $dni);
		}
		
		public function couldDeleted($dni) {
			$this->carerManagement->couldDeleted($dni);
		}
		
		public function delete($dni) {
			if ($_SESSION['debug']) {
				echo "Delete a carer: DNI:" . $dni . "<br>";
			}
			
			$this->carerManagement->deleteCarer($dni);
		}
		
		public function getSchedule() {
			$this->carerManagement->getSchedule();
		}

		public function addTurn($dni, $initTimestamp, $endTimestamp, $day) {
			$this->carerManagement->addTurn($dni, $initTimestamp, $endTimestamp, $day);
		}
		
		public function modifyTurn($newDni,$initTimestamp,$newEndTimestamp, $day, $newInitTimestamp) {
			$this->carerManagement->modifyTurn($newDni,$initTimestamp,$newEndTimestamp, $day, $newInitTimestamp);
		}
	}
?>