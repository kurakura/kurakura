<?php
	include_once '../DomainLayer/Controllers/AccessManagement.php';
	
	class AccessAction {
					
		private $accessManagement;
		
		public function __construct() {				
			$this->accessManagement = new AccessManagement();
		}
	
		public function login($username, $password) {
			$this->accessManagement->login($username, $password);
		}
		
		public function logout() {
			$this->accessManagement->logout();
		}	
	}
?>