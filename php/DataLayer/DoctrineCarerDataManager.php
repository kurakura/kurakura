<?php

include_once '../DataLayer/DoctrineAbstractDataManager.php';
include_once '../DomainLayer/DataInterfaces/ICarerDataManager.php';

class DoctrineCarerDataManager extends DoctrineAbstractDataManager implements ICarerDataManager
{

    function __construct() {
        parent::__construct();
    }

    function getCarer($dni)
    {
        $queryString = 'SELECT c FROM entities\Carer c WHERE c.dni=?1';
        return $this->em->createQuery($queryString)->setParameter(1, $dni)->getSingleResult();
    }

    function exists($id)
    {
        //No se utiliza por lo tanto no esta implementada
        return null;
    }

    function getAllCarers()
    {
        //No se utiliza por lo tanto no esta implementada
        return null;
    }
}

?>
