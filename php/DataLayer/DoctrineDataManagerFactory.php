<?php

include_once '../DomainLayer/Factories/DataManagerFactory.php';
include_once '../DataLayer/DoctrineCareerDataManager.php';
include_once '../DataLayer/DoctrineResidentDataManager.php';
include_once '../DataLayer/DoctrineServiceDataManager.php';
include_once '../DataLayer/DoctrineCityDataManager.php';
include_once '../DataLayer/DoctrineContractorDataManager.php';


class DoctrineDataManagerFactory extends DataManagerFactory
{

    public function getCarerDataManager()
    {
        return new DoctrineCarerDataManager();
    }

    public function getResidentDataManager()
    {
        return new DoctrineResidentDataManager();
    }

    public function getServiceDataManager()
    {
        return new DoctrineServiceDataManager();
    }

    public function getCityDataManager()
    {
        return new DoctrineCityDataManager();
    }

    public function getContractorDataManager() {
        return new DoctrineContractorDataManager();
    }
}

echo 'ola k ase';

?>
