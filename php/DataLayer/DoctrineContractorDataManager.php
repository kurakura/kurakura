<?php
include_once '../DataLayer/DoctrineAbstractDataManager.php';
include_once '../DomainLayer/DataInterfaces/IContractorDataManager.php';

class DoctrineContractorDataManager extends DoctrineAbstractDataManager implements IContractorDataManager
{

    function __construct() {
        parent::__construct();
    }

    function getContractor($dni)
    {
        $queryString = 'SELECT co FROM entities\Contractor co WHERE co.dni=?1';
        return parent::$em->createQuery($queryString)->setParameter(1, $dni)->getSingleResult();
    }
}

?>
