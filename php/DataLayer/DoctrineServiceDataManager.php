<?php

include_once '../DataLayer/DoctrineAbstractDataManager.php';
include_once '../DomainLayer/DataInterfaces/IServiceDataManager.php';

class DoctrineServiceDataManager extends DoctrineAbstractDataManager implements IServiceDataManager
{

    function __construct() {
        parent::__construct();
    }

    /**
     * @param $id
     * @return entities\Service
     */
    function getService($id)
    {
        $queryString = 'SELECT s FROM entities\Service s WHERE s.id=?1';
        return $this->em->createQuery($queryString)->setParameter(1, $id)->getSingleResult();
    }

    function exists($id)
    {
        //No se utiliza por lo tanto no esta implementada
        return null;
    }

    function getAllServices()
    {
        $query = parent::$em->createquery("SELECT s FROM entities\Service s");
        return $query->getResult();
    }

}
?>
