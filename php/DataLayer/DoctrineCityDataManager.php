<?php

include_once '../DataLayer/DoctrineAbstractDataManager.php';
include_once '../DomainLayer/DataInterfaces/ICityDataManager.php';

class DoctrineCityDataManager extends DoctrineAbstractDataManager implements ICityDataManager
{

    function __construct() {
        parent::__construct();
    }

    function getCity($name)
    {
        $queryString = 'SELECT c FROM entities\City c WHERE c.name=?1';
        return parent::$em->createQuery($queryString)->setParameter(1, $name)->getSingleResult();
    }
}

?>
