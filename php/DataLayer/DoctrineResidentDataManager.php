<?php
include_once '../DataLayer/DoctrineAbstractDataManager.php';
include_once '../DomainLayer/DataInterfaces/IResidentDataManager.php';

class DoctrineResidentDataManager extends DoctrineAbstractDataManager implements IResidentDataManager
{

    function __construct() {
        parent::__construct();
    }

    function getResident($dni)
    {
        $queryString = 'SELECT r FROM entities\Resident r WHERE r.dni=?1';
        $resident = $this->em->createQuery($queryString)->setParameter(1, $dni)->getSingleResult();
    }

    function exists($dni)
    {
        //No se utiliza por lo tanto no esta implementada
        return null;
    }

    function getAllResidents()
    {
        //No se utiliza por lo tanto no esta implementada
        return null;
    }
}
?>
