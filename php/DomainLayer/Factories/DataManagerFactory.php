<?php
include_once '../DomainLayer/IDataManagerFactory.php';


class DataManagerFactory implements IDataManagerFactory
{

    private static $instance;

    public static function getInstance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new DataManagerFactory();
        }
        return $inst;
    }

    /**
     * Private ctor so nobody else can instance it
     *
     */
    private function __construct()
    {

    }

    public function getCarerDataManager()
    {
        return new DoctrineCarerDataManager();
    }

    public function getResidentDataManager()
    {
        return new DoctrineResidentDataManager();
    }

    public function getServiceDataManager()
    {
        return new DoctrineServiceDataManager();
    }

    public function getCityDataManager()
    {
        return new DoctrineCityDataManager();
    }

    public function getContractorDataManager() {
        return new DoctrineContractorDataManager();
    }

}

?>
