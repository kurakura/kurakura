<?php

interface IDataManagerFactory
{

    function getCarerDataManager();

    function getResidentDataManager();

    function getServiceDataManager();

    function getCityDataManager();

    function getContractorDataManager();

}

?>
