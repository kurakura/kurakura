<?php

interface ICarerDataManager
{

    function getCarer($dni);

    function exists($dni);

    function getAllCarers();

}
?>
