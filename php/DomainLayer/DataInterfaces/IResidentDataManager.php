<?php

interface IResidentDataManager
{

    public function getResident($dni);

    public function exists($dni);

    public function getAllResidents();

}
?>
