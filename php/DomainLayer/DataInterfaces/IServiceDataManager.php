<?php

include_once dirname(__FILE__) . '../../libs/model/entities/Service.php';

interface IServiceDataManager
{

    /**
     * @param $id
     * @return Service
     */
    function getService($id);

    function exists($id);

    function getAllServices();

}
?>
