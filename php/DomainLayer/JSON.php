<?php	
	class JSON {
		//Encode 5.4
		public static function b_encode($array) {
			return urldecode(json_encode($array));
		}
		
		//Encode 5.3.3
		public static function encode($array) {
			$arrayJson = array();
			foreach($array as $item) {
				array_push($arrayJson, $item->jsonSerialize());
			}
			
			return urldecode(json_encode($arrayJson));
		}
		
		public static function simpleEncode($array) {			
			return urldecode(json_encode($array));
		}
		
		public static function singleEncode($object) {
			return urldecode(json_encode($object->jsonSerialize()));
		}
	}
?>