<?php
	include_once '../DomainLayer/JSON.php';
	include_once '../../libs/orm/erm.php';
	
	/**
	 * @author Lorena
	 * @version 1.0
	 * @created 01-nov-2012 18:52:12
	 */
	class CarerManagement {
		
		private $em;
		private $carersColor;
	
		function __construct() {
			$this->em = EntityManagerFactory::createEntityManager();
			$this->carersColor = array("#9AA6FF","#E8C196","#77FFCE","#E8A4E3","#DEFF89","#BC9EE8","#93FFA0","#E8928F","#FFF198");
			
		}
	
		public function __destruct() {
		}
	
		/**
		 * 
		 * @param channel
		 * @param email
		 * @param phone
		 * @param surname
		 * @param name
		 * @param dni
		 */
		public function addCarer($isResponsible, $channel, $email, $phone, $username, $surname, $name, $dni) {
			if ($_SESSION['debug']) {
				echo '[Domain]';
			}
			try {
				$queryString = 'SELECT s FROM entities\Service s WHERE s.id=?1';
				$service = $this->em->createQuery($queryString)->setParameter(1, $_SESSION['selectedService'])->getSingleResult();
				
				$queryString = 'SELECT COUNT(c.dni) FROM entities\Carer c WHERE c.service=?1';
				$countCarers = $this->em->createQuery($queryString)->setParameter(1, $service)->getSingleResult();
			
				if ($countCarers['1'] == "0") {
					$isResponsible = true;
				}
					
				$carer = new entities\Carer($dni, $name, $surname, $email, $phone, $username, 'pass', $channel, $isResponsible, $service);
	
				$this->em->persist($carer);
				$this->em->flush();
				
				if ($countCarers['1'] == "0") {
					$this->createFirstTurns($dni);
				}
	
			} catch (Exception $exc) {
				echo $exc->getMessage();
			}
		}
		
		public function listCarers($all) {
			try {			
				$queryString = 'SELECT s FROM entities\Service s WHERE s.id=?1';
				$service = $this->em->createQuery($queryString)->setParameter(1, $_SESSION['selectedService'])->getSingleResult();
				
				if ($all == "1") {
					$queryString = 'SELECT c FROM entities\Carer c WHERE c.service=?1';
					$carers = $this->em->createQuery($queryString)->setParameter(1, $service)->getResult();
					$index = 0;
				} else {
					$queryString = 'SELECT c FROM entities\Carer c WHERE c.isResponsible=false AND c.service=?1';
					$carers = $this->em->createQuery($queryString)->setParameter(1, $_SESSION['selectedService'])->getResult();
					$index = 1;
				}
				
				$carersJson = array();
				
				foreach($carers as $carer) {
					$carerEdit['color'] = $this->carersColor[$index];
					$carerEdit['dni'] = $carer->getDni();
					$carerEdit['name'] = $carer->getName();
					$carerEdit['surname'] = $carer->getSurname();
					$carerEdit['email'] = $carer->getEmail();
					$carerEdit['phone'] = $carer->getPhone();
					$carerEdit['username'] = $carer->getUsername();
					$carerEdit['channel'] = $carer->getChannel();
					$carerEdit['isResponsible'] = $carer->isResponsible();
					array_push($carersJson, $carerEdit);
					$index++;
				}
						
				echo JSON::simpleEncode($carersJson);				
			} catch (Exception $exc) {
				echo $exc->getMessage();
			}
		}
	
		public function getResponsible() {
			try {	
				$queryString = 'SELECT s FROM entities\Service s WHERE s.id=?1';
				$service = $this->em->createQuery($queryString)->setParameter(1, $_SESSION['selectedService'])->getSingleResult();
				
				$queryString = 'SELECT c FROM entities\Carer c WHERE c.isResponsible=true AND c.service=?1';
				$carers = $this->em->createQuery($queryString)->setParameter(1, $service)->getResult();
				
				$carersJson = array();
				
				$index = 0;
				foreach($carers as $carer) {
					$carerEdit['color'] = $this->carersColor[$index];
					$carerEdit['dni'] = $carer->getDni();
					$carerEdit['name'] = $carer->getName();
					$carerEdit['surname'] = $carer->getSurname();
					$carerEdit['email'] = $carer->getEmail();
					$carerEdit['phone'] = $carer->getPhone();
					$carerEdit['username'] = $carer->getUsername();
					$carerEdit['channel'] = $carer->getChannel();
					$carerEdit['isResponsible'] = $carer->isResponsible();
					array_push($carersJson, $carerEdit);
					$index++;
				}
						
				echo JSON::simpleEncode($carersJson);	
				
			} catch (Exception $exc) {
				echo $exc->getMessage();
			}
		}
	
		/**
		 * 
		 * @param channel
		 * @param email
		 * @param phone
		 * @param surname
		 * @param name
		 * @param dni
		 */
		public function modifyCarer($channel, $email, $phone, $username, $surname, $name, $dni) {
			try {
				$queryString = 'SELECT c FROM entities\Carer c WHERE c.dni=?1';
				$carer = $this->em->createQuery($queryString)->setParameter(1, $dni)->getSingleResult();
				
				$carer->setName($name);
				$carer->setSurname($surname);
				$carer->setUserName($username);
				$carer->setEmail($email);
				$carer->setPhone($phone);
				$carer->setChannel($channel);
				
				$this->em->merge($carer);
				$this->em->flush();			
				
			} catch (Exception $exc) {
				echo $exc->getMessage();
			}
		}
		
		/**
		 * 
		 * @param dni
		 */
		public function couldDeleted($dni) {
			try {
				$queryString = 'SELECT c.isResponsible FROM entities\Carer c WHERE c.dni=?1';
				$carerResponsibility = $this->em->createQuery($queryString)->setParameter(1, $dni)->getSingleResult();
				
				if ($carerResponsibility['isResponsible'] == 1)
					echo "false";
				else 
					echo "true";				
			
			} catch (Exception $exc) {
				echo $exc->getMessage();
			}
		}
	
		/**
		 * 
		 * @param dni
		 */
		public function deleteCarer($dni) {
			try {
				//Get Carer
				$queryString = 'SELECT c FROM entities\Carer c WHERE c.dni=?1';
				$carer = $this->em->createQuery($queryString)->setParameter(1, $dni)->getSingleResult();
				
				//Get Responsible
				$queryString = 'SELECT s FROM entities\Service s WHERE s.id=?1';
				$service = $this->em->createQuery($queryString)->setParameter(1, $_SESSION['selectedService'])->getSingleResult();
				
				$queryString = 'SELECT c FROM entities\Carer c WHERE c.isResponsible=true AND c.service=?1';
				$carers = $this->em->createQuery($queryString)->setParameter(1, $service)->getResult();
				
				//Get Turns of the carer
				$queryString = 'SELECT t FROM entities\Turn t WHERE t.carer=?1';
				$turns = $this->em->createQuery($queryString)->setParameters(array( 1 => $carer))->getResult();
				
				//Assign to Turns to Responsible
				foreach($turns as $turn) {
					$turn->setCarer($carers[0]);
				
					$this->em->persist($turn);
					$this->em->flush();
				}
				
				//Delete carer
				$queryString = 'DELETE FROM entities\Carer c WHERE c.dni=?1';
				$this->em->createQuery($queryString)->setParameter(1, $dni)->execute();				
				
			} catch (Exception $exc) {
				echo $exc->getMessage();
			}
		}
		
		public function getSchedule() {
			try {	
				$queryString = 'SELECT s FROM entities\Service s WHERE s.id=?1';
				$service = $this->em->createQuery($queryString)->setParameter(1, $_SESSION['selectedService'])->getSingleResult();
				
				$queryString = 'SELECT c FROM entities\Carer c WHERE c.service=?1';
				$carers = $this->em->createQuery($queryString)->setParameter(1, $service)->getResult();
				
				$queryString = 'SELECT t FROM entities\Turn t LEFT JOIN entities\Carer c WITH t.carer=c WHERE c.service=?1 ORDER BY t.day, t.startHour';
				$turns = $this->em->createQuery($queryString)->setParameter(1, $service)->getResult();
				
				$turnsJson = array();
				$foundCarer = false;
				
				foreach($turns as $turn) {
					$index = 0;
					foreach($carers as $carer) {
						if ($turn->getCarer() == $carer) {
							$foundCarer=true;
							break;
						}
						$index++;
					}
					if ($foundCarer)
					$turnEdit['color'] = $this->carersColor[$index];
					else
					$turnEdit['color'] = "#000";
					$turnEdit['dni'] = $turn->getCarer()->getDni();
					$turnEdit['startTime'] = $turn->getStartHour();
					$turnEdit['endTime'] = $turn->getEndHour();
					$turnEdit['day'] = $turn->getDay();
					array_push($turnsJson, $turnEdit);
					
				}
						
				echo JSON::simpleEncode($turnsJson);
				
			} catch (Exception $exc) {
				echo $exc->getMessage();
			}
		}
	
		/**
		 * 
		 * @param turn
		 */
		public function addTurn($dni,$initTimestamp,$endTimestamp, $day) {
			try {
				$queryString = 'SELECT c FROM entities\Carer c WHERE c.dni=?1';
				$carer = $this->em->createQuery($queryString)->setParameter(1, $dni)->getSingleResult();
				
				$turn = new entities\Turn($carer, $day, $initTimestamp,$endTimestamp);
				
				$this->em->persist($turn);
				$this->em->flush();
				
			} catch (Exception $exc) {
				echo $exc->getMessage();
			}
		}
	
		/**
		 * 
		 * @param turns
		 */
		public function modifyTurn($newDni,$initTimestamp,$newEndTimestamp, $day, $newInitTimestamp) {
			try {
				$queryString = 'SELECT s FROM entities\Service s WHERE s.id=?1';
				$service = $this->em->createQuery($queryString)->setParameter(1, $_SESSION['selectedService'])->getSingleResult();


				$queryString = 'SELECT t FROM entities\Turn t LEFT JOIN entities\Carer c WITH t.carer=c WHERE c.service=?3 AND t.startHour=?1 AND t.day=?2';
				$turn = $this->em->createQuery($queryString)->setParameters(array( 1 => $initTimestamp, 2 => $day, 3 => $service))->getSingleResult();
					
				if ($newDni != "-1") {
					$queryString = 'SELECT c FROM entities\Carer c WHERE c.dni=?1';
					$carer = $this->em->createQuery($queryString)->setParameter(1, $newDni)->getSingleResult();
					$turn->setCarer($carer);
				} 
								
				$turn->setDay($day);
				$turn->setHourRange($newInitTimestamp, $newEndTimestamp);
				
				$this->em->merge($turn);
				$this->em->flush();
				
			} catch (Exception $exc) {
				echo $exc->getMessage();
			}
		}
		
		/**
		 * 
		 * @param turn
		 */
		/*public function deleteTurn(Tuple{dni:String,turn:Tuple{initTimestamp:Integer,endTimestamp:Integer} $turn) {
		}*/
		
		private function createFirstTurns($carerDni) {
			for ($i = 0; $i < 7; $i++) {
				for ($j = 0; $j < 3; $j++)
					$this->addTurn($carerDni,$j * 8,$j * 8 + 8, $i);
			}
		}
	
	}
?>