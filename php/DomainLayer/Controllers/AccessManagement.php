<?php
	include_once '../DomainLayer/JSON.php';
	include_once '../../libs/orm/erm.php';
	
	/**
	 * @author Lorena
	 * @version 1.0
	 * @created 01-nov-2012 18:51:51
	 */
	class AccessManagement {
		private $em;
		
		function __construct() {
			$this->em = EntityManagerFactory::createEntityManager();
		}
	
		function __destruct() {
		}
	
		/**
		 * 
		 * @param password
		 * @param username
		 */
		public function login($username, $password) {
			try {
				$queryString = 'SELECT p FROM entities\Persona p WHERE p.username=?1 AND p.password=?2';
				$user = $this->em->createQuery($queryString)->setParameters(array(1 => $username, 2 => $password))->getSingleResult();
				
				$type;
				if ($user instanceof entities\Admin){
					$type = "admin";
				} else if($user instanceof entities\Beneficiary) {
					$type = "beneficiary";
									
					if ($user instanceof entities\Carer){
						$type = "carer";
						if ($user->isResponsible())
							$type = "responsible";
					}
					
				} else if($user instanceof entities\Resident) {
					$type = "resident";
				}
				
				if ($type != "admin")
					$_SESSION['selectedService'] = $user->getService()->getId();
										
				$registeredUser['name'] = $user->getName();
				$registeredUser['surname'] = $user->getSurname();
				$registeredUser['type'] = $type;
				
				$_SESSION['debug'] = false;
				$_SESSION['registeredUser'] = $registeredUser;
				
				echo '{"message": "true", "type": "' . $type . '"}';
				//echo JSON::simpleEncode($userJson);
				
			} catch (Exception $exc) {
				//echo $exc->getMessage();
				echo '{"message": "false"}';
			}
		}
	
		public function logout() {
			if (session_destroy())
				echo '{"message": "true"}';
			else
				echo '{"message": "false"}';
		}
	}
?>
