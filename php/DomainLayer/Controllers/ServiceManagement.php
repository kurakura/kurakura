<?php
include_once '../DomainLayer/JSON.php';
include_once '../../libs/orm/erm.php';

/**
 * @author Lorena
 * @version 1.0
 * @created 01-nov-2012 18:54:42
 */
class ServiceManagement {
    protected static $em;

    function __construct() {
        self::$em = EntityManagerFactory::createEntityManager();
    }

    function __destruct() {
    }

    public function getSelectedServiceData() {
        try {
            $queryString = "SELECT u.id, cc.dni, cc.name, cc.surname, cc.phone, cc.bankAccount, a.phone as sphone, a.address, ci.name as city, ci.zipCode as cp, p.name as province
                                FROM entities\Service u
                                LEFT JOIN entities\Contract c WITH u = c.service
                                LEFT JOIN entities\Contractor cc WITH c.contractor = cc
                                LEFT JOIN entities\Address a WHERE a.service = u
                                LEFT JOIN a.city ci
                                LEFT JOIN ci.province p
                                WHERE u.id = ?1";

            $selectedService = self::$em->createQuery($queryString)->setParameter(1, $_SESSION['selectedService'])->getSingleResult();

            echo JSON::simpleEncode($selectedService);
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    //2Delete
    /**
     *
     * @param id
     */
    public function getService($id) {
        $queryString = 'SELECT s FROM entities\Service s WHERE s.id = ?1';
		return self::$em->createQuery($queryString)->setParameter(1, $id)->getSingleResult();
    }
}
?>