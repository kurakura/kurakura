<?php
	include_once 'ServiceManagement.php';
	
	/**
	 * @author Lorena
	 * @version 1.0
	 * @created 01-nov-2012 18:51:55
	 */
	class AdminServiceManagement extends ServiceManagement {
			
		function __construct() {
			parent::__construct();
		}
	
		function __destruct() {
		}
	
		/**
		 * 
		 * @param dni
		 * @param name
		 * @param surname
		 * @param contractorPhone
		 * @param bankAccount
		 * @param CP
		 * @param city
		 * @param address
		 * @param phone
		 */
		public function addService($dni, $name, $surname, $contractorPhone, $bankAccount, $CP, $city, $address, $phone)	{
			try {
				$service = new entities\Service(false);
				parent::$em->persist($service);
				
				$queryString = 'SELECT c FROM entities\City c WHERE c.name=?1';
				$cityIbiza = parent::$em->createQuery($queryString)->setParameter(1, $city)->getSingleResult();
	
				$addressObj = new entities\Address($address, $phone, $cityIbiza, $service);
				parent::$em->persist($addressObj);
							
				$contractor = new entities\Contractor($dni, $name, $surname, null, $contractorPhone, null, null, $bankAccount);
				parent::$em->persist($contractor);
				
				$contract = new entities\Contract();
				$contract->setService($service);
				$contract->setContractor($contractor);
				parent::$em->persist($contract);
				
				parent::$em->flush();
				
				$_SESSION['selectedService'] = $service->getId();
				
				echo $service->getId();
				
			} catch (Exception $exc) {
				echo $exc->getMessage();
			}
		}
		
		public function listServices() {
			try {		
				$query = parent::$em->createquery("SELECT u.id, cc.name, cc.surname, a.phone FROM entities\Service u LEFT JOIN entities\Contract c WITH u = c.service LEFT JOIN entities\Contractor cc WITH c.contractor = cc JOIN entities\Address a WHERE a.service = u");
				$services = $query->getResult();
				
				$servicesJson = array();
				foreach($services as $service) {
					$serviceEdit['id'] = $service['id'];
					$serviceEdit['name'] = $service['name'] . ' ' . $service['surname'];
					$serviceEdit['phone'] = $service['phone'];
					array_push($servicesJson, $serviceEdit);
				}
						
				echo JSON::simpleEncode($servicesJson);
			} catch (Exception $exc) {
				echo $exc->getMessage();
			}
		}
		
		public function listProvinces() {
			try {
				$queryString = 'SELECT p FROM entities\Province p';
				$provinces = parent::$em->createQuery($queryString)->getResult();
				
				echo JSON::encode($provinces);
			} catch (Exception $exc) {
				echo $exc->getMessage();
			}
		}
		
		/**
		 * 
		 * @param province
		 */
		public function listCities($province) {
			try {
				$queryString = 'SELECT p FROM entities\Province p WHERE p.name=?1';
				$provinceObject = parent::$em->createQuery($queryString)->setParameter(1, $province)->getSingleResult();
				
				echo JSON::encode($provinceObject->getCities());
			} catch (Exception $exc) {
				echo $exc->getMessage();
			}
		}
	
		/**
		 * 
		 * @param dni
		 * @param name
		 * @param surname
		 * @param contractorPhone
		 * @param bankAccount
		 * @param phone
		 */
		public function modifySelectedService($dni, $name, $surname, $contractorPhone, $bankAccount, $phone) {
			try {
				$queryString = 'SELECT co FROM entities\Contractor co WHERE co.dni=?1';
				$contractor = parent::$em->createQuery($queryString)->setParameter(1, $dni)->getSingleResult();
				
				$queryString = 'SELECT s FROM entities\Service s WHERE s.id=?1';
				$selectedService = parent::$em->createQuery($queryString)->setParameter(1, $_SESSION['selectedService'])->getSingleResult();
				
				$queryString = 'SELECT a FROM entities\Address a WHERE a.service=?1';
				$address = parent::$em->createQuery($queryString)->setParameter(1, $selectedService)->getSingleResult();
				
				$contractor->setName($name);
				$contractor->setSurname($surname);
				$contractor->setPhone($contractorPhone);
				$contractor->setBankAccount($bankAccount);
				parent::$em->merge($contractor);
				
				$address->setPhone($phone);			
				parent::$em->merge($address);
							
				parent::$em->flush();
			} catch (Exception $exc) {
				echo $exc->getMessage();
			}
		}
		
		public function deleteSelectedService() {
			try {
				$queryString = 'DELETE FROM entities\Service s WHERE s.id=?1';
				parent::$em->createQuery($queryString)->setParameter(1, $_SESSION['selectedService'])->execute();
			} catch (Exception $exc) {
				echo $exc->getMessage();
			}			
		}
	
	}
?>