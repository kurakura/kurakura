<?php
	include '../DomainLayer/JSON.php';
	include '../../libs/orm/erm.php';
	
	/**
	 * @author Lorena
	 * @version 1.0
	 * @created 01-nov-2012 18:54:32
	 */
	class ResidentManagement {
		private $title;
		private $tabulator;
		private $em;
	
		function __construct() {
			$this->em = EntityManagerFactory::createEntityManager();
			$this->title = '<strong>[Domain ResidentManagement]</strong><br>';
			$this->tabulator = '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp';
		}
	
		function __destruct() {
		}
		
		/**
		 * 
		 * @param dni
		 * @param name
		 * @param lastName
		 * @param birthDate
		 * @param phone
		 * @param email
		 * @param medicalNotes
		 */
		public function addResident($dni, $name, $surname, $birthdate, $username, $phone, $email, $medicalNotes){
			if ($_SESSION['debug']) {
				echo $this->title .
					'<strong>addResident</strong> {<br>'.
					$this->tabulator . 'DNI: ' . $dni . '<br>' .
					$this->tabulator . 'Name: ' . $name . '<br>' .
					$this->tabulator . 'Last name: ' . $surname . '<br>' .
					$this->tabulator . 'Birthdate: ' . $birthdate . '<br>' .
					$this->tabulator . 'username: ' . $username . '<br>' .
					$this->tabulator . 'Phone: ' . $phone . '<br>' .
					$this->tabulator . 'E-mail: ' . $email . '<br>' .
					$this->tabulator . 'Medical notes: ' . $medicalNotes . '<br>
				}<br><br>';
			}
			
			try {
				$queryString = 'SELECT s FROM entities\Service s WHERE s.id=?1';
				$selectedService = $this->em->createQuery($queryString)->setParameter(1, $_SESSION['selectedService'])->getSingleResult();
				
				$resident = new entities\Resident($dni, $name, $surname, $email, $phone, $username, 'pass', $medicalNotes, new \DateTime($birthdate), $selectedService);
				/*$resident->setDni($dni);
				$resident->setName($name);
				$resident->setSurname($surname);
				$resident->setBirthdate(new \DateTime($birthdate));
				$resident->setUsername($username);
				$resident->setEmail($email);
				$resident->setPhone($phone);
				$resident->setMedicalNotes($medicalNotes);
				$resident->setService($selectedService);*/
				
				$this->em->persist($resident);
				$this->em->flush();
				
			} catch (Exception $exc) {
				echo $exc->getMessage();
			}
		}
		
		public function getResident($dni) {
			try {
				$queryString = 'SELECT r FROM entities\Resident r WHERE r.dni=?1';
				$resident = $this->em->createQuery($queryString)->setParameter(1, $dni)->getSingleResult();
				
				echo JSON::singleEncode($resident);
			} catch (Exception $exc) {
				echo $exc->getMessage();
			}
		}
		
		public function listResidents() {
			try {
				$queryString = 'SELECT s FROM entities\Service s WHERE s.id=?1';
				$selectedService = $this->em->createQuery($queryString)->setParameter(1, $_SESSION['selectedService'])->getSingleResult();
				
				$queryString = 'SELECT r FROM entities\Resident r WHERE r.service=?1';
				$residents = $this->em->createQuery($queryString)->setParameter(1, $selectedService)->getResult();

				$residentsJson = array();
				foreach($residents as $resident) {
					$residentEdit['dni'] = $resident->getDni();
					$residentEdit['name'] = $resident->getName();
					$residentEdit['surname'] = $resident->getSurname();
					$residentEdit['email'] = $resident->getEmail();
					$residentEdit['phone'] = $resident->getPhone();
					$residentEdit['username'] = $resident->getUsername();
					$birthDate = explode("-", $resident->getBirthdate()->format('d-m-Y'));
					$age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y")-$birthDate[2])-1):(date("Y")-$birthDate[2]));					
					$residentEdit['age'] = $age;
					$residentEdit['medicalNotes'] = $resident->getMedicalNotes();
					array_push($residentsJson, $residentEdit);
				}
						
				echo JSON::simpleEncode($residentsJson);
			} catch (Exception $exc) {
				echo $exc->getMessage();
			}
		}
	
		/**
		 * 
		 * @param dni
		 * @param name
		 * @param lastName
		 * @param birthDate
		 * @param phone
		 * @param email
		 * @param medicalNotes
		 */
		public function modifiyResident($dni, $name, $surname, $birthdate, $phone, $email, $medicalNotes) {
			if ($_SESSION['debug']) {
				echo $this->title .
					'<strong>modifiyResident</strong> {<br>'.
					$this->tabulator . 'DNI: ' . $dni . '<br>' .
					$this->tabulator . 'Name: ' . $name . '<br>' .
					$this->tabulator . 'Last name: ' . $lastName . '<br>' .
					$this->tabulator . 'Birthdate: ' . $birthdate . '<br>' .
					$this->tabulator . 'Phone: ' . $phone . '<br>' .
					$this->tabulator . 'E-mail: ' . $email . '<br>' .
					$this->tabulator . 'Medical notes: ' . $medicalNotes . '<br>
				}<br><br>';
			}
			
			try {
				$queryString = 'SELECT r FROM entities\Resident r WHERE r.dni=?1';
				$resident = $this->em->createQuery($queryString)->setParameter(1, $dni)->getSingleResult();
				
				$resident->setName($name);
				$resident->setSurname($surname);
				//$resident->setUserName($username);
				$resident->setEmail($email);
				$resident->setPhone($phone);
				$resident->setMedicalNotes($medicalNotes);
				
				$this->em->merge($resident);
				$this->em->flush();
			} catch (Exception $exc) {
				echo $exc->getMessage();
			}
		}
		
		/**
		 * 
		 * @param dni
		 */
		public function deleteResident($dni) {
			if ($_SESSION['debug']) {
				echo $this->title . 
					'<strong>delete</strong> DNI: ' . $dni . '<br><br>';
			}
			
			try {
				$queryString = 'DELETE FROM entities\Resident r WHERE r.dni=?1';
				$this->em->createQuery($queryString)->setParameter(1, $dni)->execute();
			} catch (Exception $exc) {
				echo $exc->getMessage();
			}
		}
	}
?>