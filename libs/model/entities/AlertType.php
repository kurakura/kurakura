<?php

namespace entities;

/**
 * @Entity 
 * @Table("AlertType")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="type", type="string")
 * @DiscriminatorMap({"alert" = "AlertType", "custom" = "CustomAlertType"})
 */
class AlertType
{
	/**
	 * @Id
	 * @Column(type="string", length=50)
	 */
	private $name;
	
	/**
	 * @Column(type="string", length=10)
	 */
	private $risk;
	
	public function getName() {
		return $this->name;
	}
	
	public function setName($name) {
		$this->name = $name;
	}
	
	public function getRisk() {
		return $this->risk;
	}
	
	public function setRisk($risk) {
		$this->risk = $risk;
	}
}
?>