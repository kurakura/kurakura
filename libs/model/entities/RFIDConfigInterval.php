<?php

namespace entities;
/**
 * @Entity 
 * @Table(name="RFIDConfigInterval")
 */
class RFIDConfigInterval extends Configuration
{
	/**
	 * @Column(type="time") 
	 */
	private $startHour;
	
	/**
	 * @Column(type="time") 
	 */
	private $endHour;
	
	public function getStartHour() {
		return $this->startHour;
	}
	
	public function getEndHour() {
		return $this->endHour;
	}
	
	public function setHourRange($start, $end) {
		$this->startHour = $start;
		$this->endHour = $end;
	}
}
?>