<?php

namespace entities;

/**
 * @Entity
 * @Table(name="Persona")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="type", type="string")
 * @DiscriminatorMap({"persona" = "Persona", "beneficiary" = "Beneficiary", "contractor" = "Contractor",
 * "admin" = "Admin", "operator" = "Operator", "carer" = "Carer", "resident" = "Resident"})
 * */
class Persona
{
	/** 
	 * @Id
	 * @Column(type="string", length=10)
	 *  */
	private $dni;
	
	/** @Column(type="string", length=10) */
	private $name;
	
	/** @Column(type="string", length=50, nullable=true) */
	private $surname;
	
	/** @Column(type="string", length=50, nullable=true) */
	private $email;
	
	/** @Column(type="integer", nullable=true) */
	private $phone;
	
	/** @Column(type="string", length=50, nullable=true, unique=true) */
	private $username;
	
	/** @Column(type="string", length=50, nullable=true) */
	private $password;
	
	function __construct($dni, $name, $surname, $email, $phone, $username, $password) {
		$this->dni = $dni;
		$this->name = $name;
		$this->surname = $surname;
		$this->email = $email;
		$this->phone = $phone;
		$this->username = $username;
		$this->password = $password;
	}
	
	public function jsonSerialize() {	
        return array(
			'dni'=>$this->dni, 
			'name'=>$this->name,
			'surname'=>$this->surname,
			'email'=>$this->email,
			'phone'=>$this->phone,
			'username'=>$this->username
		);
    }
	
	public function getDni() {
		return $this->dni;
	} 
	
	public function setDni($dni) {
		$this->dni = $dni;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function setName($name) {
		$this->name = $name;
	}
	
	public function getSurname() {
		return $this->surname;
	}
	
	public function setSurname($surname) {
		$this->surname = $surname;
	}
	
	public function getEmail() {
		return $this->email;
	}
	
	public function setEmail($email) {
		$this->email = $email;
	}
	
	public function getPhone() {
		return $this->phone;
	}
	
	public function setPhone($phone) {
		$this->phone = $phone;
	}
	
	public function getUsername() {
		return $this->username;
	}
	
	public function setUsername($username) {
		$this->username = $username;
	}
	
	public function getPassword() {
		return $this->password;
	}
	
	public function setPassword($password) {
		$this->password = $password;
	}
	
}