<?php

namespace entities;

/**
 * @Entity
 * @Table(name="Notification")
 */
class Notification
{
	/**
	 * @Id
	 * @Column(type="integer")
	 * @GeneratedValue
	 */
	private $id;
	
	/**
	 * @ManyToOne(targetEntity="Service")
	 * @JoinColumn(name="idservice", referencedColumnName="id") 
	 */
	private $service;
	
	/**
	 * @ManyToOne(targetEntity="Device")
	 * @JoinColumn(name="iddevice", referencedColumnName="id") 
	 */
	private $device;
	
	/**
	 * @Column(type="date") 
	 */
	private $date;
	
	/**
	 * @Column(type="time") 
	 */
	private $hour;
	
	public function setId($id) {
		$this->id = $id;
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function getService() {
		return $this->service;
	}
	
	public function setService($service) {
		$this->service = $service;
	}
	
	public function getDevice() {
		return $this->device;
	}
	
	public function setDevice($device) {
		$this->device = $device;
	}
	
	public function getDay() {
		return $this->day;
	}
	
	public function setDay($day) {
		$this->day = $day;
	}
	
	public function getHour() {
		return $this->hour;
	}
	
	public function setHour($hour) {
		$this->hour = $hour;
	}
}
?>