<?php

namespace entities;

/**
 * @Entity 
 * @Table(name="City")
 */
class City
{
	/**
	 * @Id
	 * @Column(type="string", length=50) 
	 */
	private $name;
	
	/** 
	 * @Id
	 * @ManyToOne(targetEntity="Province", inversedBy="cities")
	 * @JoinColumn(name="province_name", referencedColumnName="name")  
	 */
	private $province;
	
	/**
	 *  @Column(type="string", length=10)
	 */
	private $zipCode;
	
	/**
	 * @OneToMany(targetEntity="Address", mappedBy="city") 
	 */
	private $addresses;
	
	function __construct($name, $zipCode, $province) {
		$this->addresses = new \Doctrine\Common\Collections\ArrayCollection();
		$this->name = $name;
		$this->zipCode = $zipCode;
		$this->province = $province;
	}
	
	public function jsonSerialize() {
		return array(
			'name'=>$this->name
		);
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function setName($name) {
		$this->name = $name;
	}
	
	public function getZipCode() {
		return $this->zipCode;
	}
	
	public function setZipCode($zipCode) {
		$this->zipCode = $zipCode;
	}
	
	public function getProvince() {
		return $this->province;
	}
	
	public function setProvince($province) {
		$this->province = $province;
	}
	
	public function getAddresses() {
		return $this->addresses;
	}
	
	public function addAddress($address) {
		$this->addresses->add($address);
	}
	
	public function removeAddress($address) {
		$this->addresses->removeElement($address);
	}
}
?>