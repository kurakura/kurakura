<?php

namespace entities;

/**
 * @Entity 
 * @Table(name="Resident")
 */
class Resident extends Beneficiary
{
	/** @Column(type="string", length=140, nullable=true) */
	private $medicalNotes;
	
	/** @ManyToOne(targetEntity="Service", inversedBy="residents")
	* @JoinColumn(name="idservice", referencedColumnName="id", onDelete="SET NULL") */
	private $service;
	
	/** @Column(type="date") */
	private $birthdate;
	
	function __construct($dni, $name, $surname, $email, $phone, $username, $password, $medicalNotes, $birthdate, $service) {
		parent::__construct($dni, $name, $surname, $email, $phone, $username, $password);
		$this->medicalNotes = $medicalNotes;
		if ($birthdate == null)
			$this->birthdate = new \DateTime($birthdate);
		else 
			$this->birthdate = $birthdate;
		$this->service = $service;
	}
	
	function __destruct() {
		
	}
	
	public function jsonSerialize() {	
        return array_merge(parent::jsonSerialize(), array(
			'birthdate'=>$this->birthdate->format('d-m-Y'),
			'medicalNotes'=>$this->medicalNotes
		));
    }
	
	public function getMedicalNotes() {
		return $this->medicalNotes;
	}
	
	public function setMedicalNotes($medicalNotes) {
		$this->medicalNotes = $medicalNotes;
	}
	
	public function getService() {
		return $this->service;
	}
	
	public function setService($service) {
		$this->service = $service;
	}
	
	public function getBirthDate() {
		return $this->birthdate;
	}
	
	public function setBirthDate($birthdate) {
		$this->birthdate = $birthdate;
	}
}