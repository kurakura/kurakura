<?php

namespace entities;

/**
 * @Entity
 * @Table("Reminder")
 */
class Reminder
{
	/**
	 * @Id
	 * @Column(type="integer")
	 * @GeneratedValue 
	 */
	private $id;
	
	/**
	 * @Column(type="string", length=50) 
	 */
	private $topic;
	
	/**
	 * @Column(type="string", length=50) 
	 */
	private $description;
	
	/**
	 * @Column(type="string", length=50, nullable=true)
	 */
	private $channel;
	
	/**
	 * @Column(type="date", nullable=true) 
	 */
	private $date;
	
	/**
	 * @Column(type="time", nullable=true) 
	 */
	private $hour;
	
	function __construct() {
	}
	
	public function setId($id) {
		$this->id = $id;
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function getChannel() {
		return $this->channel;
	}
	
	public function setChannel($channel) {
		$this->channel = $channel;
	}
	
	public function getTopic() {
		return $this->topic;
	}
	
	public function setTopic($topic) {
		$this->topic = $topic;
	}
	
	public function getDescription() {
		return $this->description;
	}
	
	public function setDescription($description) {
		$this->description = $description;
	}
	
	public function getDate() {
		return $this->date;
	}
	
	public function setDate($date) {
		$this->date = $date;
	}
	
	public function getHour() {
		return $this->hour;
	}
	
	public function setHour($hour) {
		$this->hour = $hour;
	}
}
?>