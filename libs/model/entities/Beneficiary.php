<?php

namespace entities;

/**
 * @Entity
 * @Table(name="Beneficiary")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="type", type="string")
 * @DiscriminatorMap({"beneficiary" = "Beneficiary", "resident" = "Resident", "carer" = "Carer"})
 */
class Beneficiary extends Persona
{	
	
	function __construct($dni, $name, $surname, $email, $phone, $username, $password) {
		parent::__construct($dni, $name, $surname, $email, $phone, $username, $password);
	}
}
?>