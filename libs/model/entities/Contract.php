<?php
namespace entities;

//He intentado que se mapeara solo, pero no ha habido forma

/**
 * @Entity 
 * @Table(name="Contract")
 */
class Contract
{
	/** 
	 * @Id
	 * @OneToOne(targetEntity="Service")
	 * @JoinColumn(name="service_id", referencedColumnName="id", onDelete="CASCADE")
	 * */
	private $service;
	
	/**
	 * @Id
	 * @OneToOne(targetEntity="Contractor")
	 * @JoinColumn(name="contractor_dni", referencedColumnName="dni", onDelete="CASCADE")
	 */
	private $contractor;
	
	function __construct() {
	}
	
	public function getService() {
		return $this->service;
	}
	
	public function setService($service) {
		$this->service = $service;
	}
	
	public function getContractor() {
		return $this->contractor;
	}
	
	public function setContractor($contractor) {
		$this->contractor = $contractor;
	}
}