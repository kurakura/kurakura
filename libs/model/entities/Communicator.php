<?php

namespace entities;

//A.K.A PersonalDevice
/**
 * @Entity
 * @Table(name="Communicator")
 */
class Communicator extends Device
{

	/**
	 * @OneToMany(targetEntity="RFIDTag", mappedBy="communicator")
	 */
    private $rfidTags;

    /**
     * @OneToMany(targetEntity="HighRiskDevice", mappedBy="communicator") 
     */
    private $highRiskDevices;

    /**
     * @OneToOne(targetEntity="Resident")
     * @JoinColumn(name="resident_dni", referencedColumnName="dni")
     */
    private $resident;

    function __construct()
    {
        $this->rfidTags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->highRiskDevices = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function setHighRiskDevices($highRiskDevices)
    {
        $this->highRiskDevices = $highRiskDevices;
    }

    public function getHighRiskDevices()
    {
        return $this->highRiskDevices;
    }

    public function setResident($resident)
    {
        $this->resident = $resident;
    }

    public function getResident()
    {
        return $this->resident;
    }

    public function setRfidTags($rfidTags)
    {
        $this->rfidTags = $rfidTags;
    }

    public function getRfidTags()
    {
        return $this->rfidTags;
    }

}
