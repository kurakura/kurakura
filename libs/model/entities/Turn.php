<?php

namespace entities;

/**
 * @Entity 
 * @Table(name="Turn")
 */
class Turn
{
	/** 
	 * @Id
	 * @ManyToOne(targetEntity="Carer")
	 * @JoinColumn(name="carer_dni", referencedColumnName="dni", onDelete="CASCADE")  
	 */
	private $carer;
	
	/**
	 * @Id
	 * @Column(type="integer")
	 */
	private $day;
	
	/**
	 * @Id
	 * @Column(type="integer")
	 */
	private $startHour;
	
	/**
	 * @Column(type="integer")
	 */
	private $endHour;
	
	function __construct($carer, $day, $startHour, $endHour) {
		$this->carer = $carer;
		$this->day = $day;
		$this->startHour = $startHour;
		$this->endHour = $endHour;
	}
	
	public function jsonSerialize() {	
       return array(
			'carer'=>$this->carer->getDni(), 
			'day'=>$this->day,
			'startTime'=>$this->startHour,
			'endTime'=>$this->endHour
		);
    }
	
	public function getCarer() {
		return $this->carer;
	}
	
	public function setCarer($carer) {
		$this->carer = $carer;
	}
	
	public function getDay() {
		return $this->day;
	}
	
	public function setDay($day) {
		$this->day = $day;
	}
	
	public function getStartHour() {
		return $this->startHour;
	}
	
	public function getEndHour() {
		return $this->endHour;
	}
	
	public function setHourRange($start, $end) {
		$this->startHour = $start;
		$this->endHour = $end;
	}
}
?>