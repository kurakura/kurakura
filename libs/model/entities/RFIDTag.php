<?php

namespace entities;
/**
 * @Entity
 * @Table(name="RFIDTag")
 */
class RFIDTag extends Device
{
	/**
	 * @ManyToOne(targetEntity="Configuration")
	 * @JoinColumn(name="customalerttype_name", referencedColumnName="customalert")
	 */
	private $configuration;
	
	/**
	 * @ManyToOne(targetEntity="Communicator", inversedBy="rfidTags")
	 * @JoinColumn(name="comm_id", referencedColumnName="id") 
	 */
	private $communicator;
	
	public function setConfiguration($configuration) {
		$this->configuration = $configuration;
	}
	
	public function getConfiguration() {
		return $this->configuration;
	}
	
	public function setCommunicator($communicator) {
		$this->communicator = $communicator;
	}
	
	public function getCommunicator() {
		return $this->communicator;
	}
}
?>