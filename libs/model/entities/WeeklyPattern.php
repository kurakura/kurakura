<?php

namespace entities;

/**
 * @Entity 
 * @Table("WeeklyPattern")
 */
class WeeklyPattern 
{
	/**
	 * @Id
	 * @OneToOne(targetEntity="Reminder")
	 * @JoinColumn(name="reminder_id", referencedColumnName="id") 
	 */
	private $reminder;
	
	/**
	 * @Column(type="integer", nullable=true)
	 */
	private $frequency;
	
	/** @Column(type="boolean") */
	private $monday;
	/** @Column(type="boolean") */
	private $tuesday;
	/** @Column(type="boolean") */
	private $wednesday;
	/** @Column(type="boolean") */
	private $thursday;
	/** @Column(type="boolean") */
	private $friday;
	/** @Column(type="boolean") */
	private $saturday;
	/** @Column(type="boolean") */
	private $sunday;
	
	function __construct() {
		$this->monday = false;
		$this->tuesday = false;
		$this->wednesday = false;
		$this->thursday = false;
		$this->friday = false;
		$this->saturday = false;
		$this->sunday = false;
	}
	
	public function getReminder() {
		return $this->reminder;
	}
	
	public function setReminder($reminder) {
		$this->reminder = $reminder;
	}
	
	public function getFrequency() {
		return $this->frequency;
	}
	
	public function setFrequency($frec) {
		$this->frequency = $frec;
	}
	
	public function getPattern() {
		return array(
			"monday"    => $this->monday,
			"tuesday"   => $this->tuesday,
			"wednesday" => $this->wednesday,
			"thursday"  => $this->thursday,
			"friday"    => $this->friday,
			"saturday"  => $this->saturday,
			"sunday"    => $this->sunday
				);
	}
	
	public function setPattern($pattern) {
		$this->monday = $pattern['monday'];
		$this->tuesday = $pattern['tuesday'];
		$this->wednesday = $pattern['wednesday'];
		$this->thursday = $pattern['thursday'];
		$this->friday = $pattern['friday'];
		$this->saturday = $pattern['saturday'];
		$this->sunday = $pattern['sunday'];
	}
}
?>