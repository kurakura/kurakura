<?php

namespace entities;

/**
 * @Entity 
 * @Table(name="Province")
 */
class Province
{
	/**
	 * @Id
	 * @Column(type="string", length=50) 
	 */
	private $name;
	
	/**
	 * @OneToMany(targetEntity="City", mappedBy="province")
	 */
	private $cities;
	
	function __construct($name) {
		$this->cities = new \Doctrine\Common\Collections\ArrayCollection();
		$this->name = $name;
	}
	
	public function jsonSerialize() {
		return array(
			'name'=>$this->name
		);
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function setName($name) {
		$this->name = $name;
	}
	
	public function getCities() {
		return $this->cities;
	}
	
	public function addCity($city) {
		$this->cities->add($city);
	}
	
	public function removeCity($city) {
		$this->cities->removeElement($city);
	}
}
?>