<?php
namespace entities;

/**
 * @Entity
 * @Table(name="Carer")
 */
class Carer extends Beneficiary
{
	/** @Column(type="string", length=50)*/
	private $channel;
	
	/** @ManyToOne(targetEntity="Service", inversedBy="carers")
	 * @JoinColumn(name="idservice", referencedColumnName="id", onDelete="SET NULL") */
	private $service;
	
	/** @Column(type="boolean") */
	private $isResponsible;
	
	function __construct($dni, $name, $surname, $email, $phone, $username, $password, $channel, $isResponsible ,$service) {
		parent::__construct($dni, $name, $surname, $email, $phone, $username, $password);
		$this->channel = $channel;
		$this->isResponsible = $isResponsible;
		$this->service = $service;
	}
	
	public function jsonSerialize() {	
        return array_merge(parent::jsonSerialize(), array(
			'channel'=>$this->channel,
			'isResponsible'=>$this->isResponsible
		));
    }
	
	public function getChannel() {
		return $this->channel;
	}
	
	public function setChannel($channel) {
		$this->channel = $channel;
	}
	
	public function getService() {
		return $this->service;
	}
	
	public function setService($service) {
		$this->service = $service;
	}
	
	public function isResponsible() {
		return $this->isResponsible;
	}
	
	public function setResponsible($resp) {
		$this->isResponsible = $resp;
	}
}

?>