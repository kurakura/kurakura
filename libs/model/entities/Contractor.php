<?php

namespace entities;

/**
 * @Entity 
 * @Table(name="Contractor")
 */
class Contractor extends Persona
{	
	/** @Column(type="string", length=50) */
	private $bankAccount;
	
	function __construct($dni, $name, $surname, $email, $phone, $username, $password, $bankAccount) {
		parent::__construct($dni, $name, $surname, $email, $phone, $username, $password);
		$this->bankAccount = $bankAccount;
	}
	
	public function jsonSerialize() {
		return array_merge(parent::jsonSerialize(), array(
				'bankAccount'=>$this->bankAccount
		));
	}
	
	public function getBankAccount() {
		return $this->bankAccount;
	}
	
	public function setBankAccount($bankAccount) {
		$this->bankAccount = $bankAccount;
	}
}

?>