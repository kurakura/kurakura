<?php

namespace entities;

/**
 * @Entity 
 * @Table(name="Admin")
 */
class Admin extends Persona
{
	function __construct($dni, $name, $surname, $email, $phone, $username, $password) {
		parent::__construct($dni, $name, $surname, $email, $phone, $username, $password);
	}
}
?>