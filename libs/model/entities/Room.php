<?php

namespace entities;

/**
 * @Entity
 * @Table(name="Room")
 */
class Room
{
	/**
	 * @Column(type="string", length=50) 
	 */
	private $name;
	
	/**
	 * @Id
	 * @ManyToOne(targetEntity="Address")
	 * @JoinColumn(name="address_id", referencedColumnName="id") 
	 */
	private $address;
	
	public function getName() {
		return $this->name;
	}
	
	public function setName($name) {
		$this->name = $name;
	}
	
	public function getAddress() {
		return $this->address;
	}
	
	public function setAddress($address) {
		$this->address = $address;
	}
}

?>