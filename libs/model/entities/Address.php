<?php

namespace entities;

/**
 * @Entity 
 * @Table(name="Address")
 */
class Address 
{
	/**
	 * @Id
	 * @Column(type="integer")
	 * @GeneratedValue 
	 */
	private $id;
	
	/**
	 * @Column(type="string", length=50) 
	 */
	private $address;

	/**
	 *	@Column(type="integer") 
	 */
	private $phone;
	
	/**
	 * @OneToOne(targetEntity="Service", inversedBy="address")
	 * @JoinColumn(name="service_id", referencedColumnName="id", onDelete = "CASCADE")
	 */
	private $service;
	
	/**
	 *	@ManyToOne(targetEntity="City", inversedBy="addresses")
	 *	@JoinColumns({
	 *		@JoinColumn(name="City_name", referencedColumnName="name"),
	 *		@JoinColumn(name="City_province", referencedColumnName="province_name")
	 *	}) 
	 */
	private $city;
	
	function __construct($address, $phone, $city, $service) {
		$this->address = $address;
		$this->phone = $phone;
		$this->city = $city;
		$this->service = $service;
	}
	
	public function setId($id) {
		$this->id = $id;
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function getAddress() {
		return $this->address;
	}
	
	public function setAddress($address) {
		$this->address = $address;
	}
	
	public function getPhone() {
		return $this->phone;
	}
	
	public function setPhone($phone) {
		$this->phone = $phone;
	}
	
	public function getService() {
		return $this->service;
	}
	
	public function setService($service) {
		$this->service = $service;
	}
	
	public function getCity() {
		return $this->city;
	}
	
	public function setCity($city) {
		$this->city = $city;
	}
}
?>