<?php

namespace entities;

/**
 * @Entity 
 * @Table(name="Device")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="devtype", type="string")
 * @DiscriminatorMap({"device" = "Device", "highrisk" = "HighRiskDevice", "communicator" = "Communicator", "rfid" = "RFIDTag"})
 */
class Device
{
	/**
	 * @Id
	 * @Column(type="integer")
	 * @GeneratedValue
	 */
    private $id;

    /**
     * @Column(type="string", length=50)
     */
    private $type;

    /**
     * @ManyToOne(targetEntity="Service")
	 * @JoinColumn(name="idservice", referencedColumnName="id") 
     */
    private $service;

    function __construct() {
    }
    
    function __construct1($type, $service) {
    	$this->type = $type;
    	$this->service = $service;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }

 /*   public function setNotifications($notifications)
    {
        $this->notifications = $notifications;
    }

    public function getNotifications()
    {
        return $this->notifications;
    }*/

    public function setService($service)
    {
        $this->service = $service;
    }

    public function getService()
    {
        return $this->service;
    }

}
