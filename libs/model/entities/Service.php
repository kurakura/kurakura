<?php

namespace entities;

/**
 * @Entity 
 * @Table(name="Service")
 */
class Service 
{
	/**
	 * @Id
	 * @Column(type="integer")
	 * @GeneratedValue 
	 */
	private $id;
	
	/** @Column(type="boolean")*/
	private $activatedRFID;
	
	/**
	 * @OneToOne(targetEntity="Address", mappedBy="service")
	 */
	private $address;
	
	/**
	 * @OneToMany(targetEntity="Resident", mappedBy="service") 
	 */
	private $residents;
	
	/**
	 * @OneToMany(targetEntity="Carer", mappedBy="service") 
	 */
	private $carers;
	
	function __construct() {
		$this->activatedRFID = false;
		$this->residents = new \Doctrine\Common\Collections\ArrayCollection();
		$this->carers = new \Doctrine\Common\Collections\ArrayCollection();
	}
	
	function __construct1($activatedRFID) {
		$this->activatedRFID = $activatedRFID;
	}
	
	public function jsonSerialize() {	
        return array(
			'id'=>$this->id,
			'activatedRFID'=>$this->activatedRFID,
        	'address'=>$this->address,
        	'residents'=>$this->residents->jsonSerialize(),
        	'carers'=>$this->carers->jsonSerialize()
		);
    }
	
	public function setId($id) {
		$this->id = $id;
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function setActivatedRFID($ar) {
		$this->activatedRFID = $ar;
	}
	
	public function getActivatedRFID() {
		return $this->activatedRFID;
	}
	
	public function getAddress() {
		return $this->address;
	}
	
	public function setAddress($address) {
		$this->address = $address;
	}
	
	public function getResidents() {
		return $this->residents;
	}
	
	public function addResident($resident) {
		$this->residents->add($resident);
	}
	
	public function deleteResident($resident) {
		$this->residents->removeElement($resident);
	}
	
	public function getCarers() {
		return $this->carers;
	}
	
	public function addCarer($carer) {
		$this->carers->add($carer);
	}
	
	public function deleteCarer($carer) {
		$this->carers->removeElement($carer);
	}
}
?>