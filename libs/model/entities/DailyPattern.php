<?php

namespace entities;

/**
 * @Entity 
 * @Table(name="DailyPattern")
 */
class DailyPattern
{
	/**
	 * @Id
	 * @OneToOne(targetEntity="Reminder")
	 * @JoinColumn(name="reminder_id", referencedColumnName="id") 
	 */
	private $reminder;
	
	/**
	 * @Column(type="integer", nullable=true) 
	 */
	private $frequency;
	
	/**
	 * @Column(type="time")
	 */
	private $startHour;
	
	/**
	 * @Column(type="time") 
	 */
	private $endHour;

	public function getReminder() {
		return $this->reminder;
	}
	
	public function setReminder($reminder) {
		$this->reminder = $reminder;
	}
	
	public function getFrequency() {
		return $this->frequency;
	}
	
	public function setFrequency($frec) {
		$this->frequency = $frec;
	}
	
	public function getStartHour() {
		return $this->startHour;
	}
	
	public function getEndHour() {
		return $this->endHour;
	}
	
	public function setHourRange($start, $end) {
		$this->startHour = $start;
		$this->endHour = $end;
	}
}
?>