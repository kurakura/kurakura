<?php

namespace entities;

/**
 * @Entity 
 * @Table(name="HighRiskDevice")
 */
class HighRiskDevice extends Device
{

	/**
	 * @ManyToOne(targetEntity="AlertType")
	 * @JoinColumn(name="alerttype_name", referencedColumnName="name")  
	 */
    private $alertType;

    /**
     * @ManyToOne(targetEntity="Communicator", inversedBy="highRiskDevices")
     * @JoinColumn(name="communicator_id", referencedColumnName="id")
     */
    private $communicator;

    public function setAlertType($alertType)
    {
        $this->alertType = $alertType;
    }

    public function getAlertType()
    {
        return $this->alertType;
    }

    public function setCommunicator($communicator)
    {
        $this->communicator = $communicator;
    }

    public function getCommunicator()
    {
        return $this->communicator;
    }

}
