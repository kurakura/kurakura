<?php

namespace entities;

/**
 * @Entity 
 * @Table(name="Configuration")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="type", type="string")
 * @DiscriminatorMap({"conf" = "Configuration", "rfidinterval" = "RFIDConfigInterval"})
 */
class Configuration
{
	
	//Aqui fuera del prototipo tendriamos un problemon
	/**
	 * @Column(type="string", length=50)		
	 */
	private $room;
	
	/**
	 * @Id
	 * @Column(type="string", length=50)
	 */
	private $customalert;
	
	/**
	 * @Column(type="boolean")
	 */
	private $presence;
	
	/**
	 * @Column(type="integer", nullable=true)
	 */
	private $duration;
	
	/**
	 * @OneToMany(targetEntity="RFIDTag", mappedBy="configuration") 
	 */
	private $tagsRFID;
	
	function __construct() {
		$this->tagsRFID = new \Doctrine\Common\Collections\ArrayCollection();
	}	
	
	function __construct1($room, $customalert, $presence, $duration) {
		$this->room = $room;
		$this->customalert = $customalert;
		$this->presence = $presence;
		$this->duration = $duration;
	}
	
	public function getRoom() {
		return $this->room;
	}
	
	public function setRoom($room) {
		$this->room = $room;
	}
	
	public function getCustomAlert() {
		return $this->customalert;
	}
	
	public function setCustomAlert($customalert) {
		$this->customalert = $customalert;
	}
	
	public function getPresence() {
		return $this->presence;
	}
	
	public function setPresence($presence) {
		$this->presence = $presence;
	}
	
	public function setDuration($duration) {
		$this->duration = $duration;
	}
	
	public function getDuration() {
		return $this->duration;
	}
	
	public function getTagsRFID() {
		return $this->tagsRFID;
	}
	
	public function addCity($rfid) {
		$this->tagsRFID->add($rfid);
	}
	
	public function removeCity($rfid) {
		$this->tagsRFID->removeElement($rfid);
	}
	
}
?>