<?php
//Sacado de http://nubedeideasparadesarrollar.blogspot.com.es/2011/02/doctrine2-parte-1-introduccion.html
require_once __DIR__ . '/doctrine-orm/Doctrine/Common/ClassLoader.php';

$classLoader = new \Doctrine\Common\ClassLoader('entities', realpath('../model'));
$classLoader->register();

$classLoader = new \Doctrine\Common\ClassLoader('Proxies', __DIR__);
$classLoader->register();
$classLoader->setNamespaceSeparator('_');

$config = new \Doctrine\ORM\Configuration();
$config->setMetadataCacheImpl(new \Doctrine\Common\Cache\ArrayCache);

//Path de las entidades
$driverImpl = $config->newDefaultAnnotationDriver(array(__DIR__ . "/../model/entities"));
$config->setMetadataDriverImpl($driverImpl);

//Path de los proxies:
$config->setProxyDir(__DIR__ . '/../model/proxies');
//Nombre del NameSpace o paquete que contendrá los proxies
$config->setProxyNamespace('proxies');

//Opciones de conexión a la base de datos
if (strcmp(gethostname(), 'ex-std-node3.prod.rhcloud.com') == 0) {
	$connectionOptions = array(
			'driver' => 'pdo_pgsql',
			'host' => getenv('OPENSHIFT_POSTGRESQL_DB_HOST'),
			'port' => getenv('OPENSHIFT_POSTGRESQL_DB_PORT'),
			'dbname' => 'www',
			'user' => 'admin',
			'password' => 'J46qz1fjhxg7'
	);
}
else {
	$connectionOptions = array(
			'driver' => 'pdo_pgsql',
			'host' => 'localhost',
			'dbname' => 'kurakura',
			'user' => 'kurakura',
			'password' => 'residenteresidente'
	);
}

$em = \Doctrine\ORM\EntityManager::create($connectionOptions, $config);

$helpers = array(
		'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($em->getConnection()),
		'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($em)
);


?>