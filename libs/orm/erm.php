<?php
require_once __DIR__ . '/doctrine-orm/Doctrine/Common/ClassLoader.php';

use Doctrine\Common\ClassLoader,
Doctrine\ORM\Configuration,
Doctrine\ORM\EntityManager,
Doctrine\Common\Cache\ApcCache;

/**
* Description of EntityManagerFactory
*
* @author Juan Carlos Perez Zapata
* @version 0.1
*/
class EntityManagerFactory {

	/**
	* @var Doctrine\ORM\EntityManager
	*/
	private static $em;

	
	/**
	*
	* @return Doctrine\ORM\EntityManager
	*/
	public static function createEntityManager() {

		if (self::$em == null) {
			//Paso 1: Cargamos las clases del namespace Doctrine
			$classLoader = new \Doctrine\Common\ClassLoader('Doctrine', __DIR__ . '/doctrine-orm');
			$classLoader->register();
			
			// Paso 2: Cargamos las entidades
			$entitiesClassLoader = new ClassLoader('entities', dirname(__DIR__).'/model');
			$entitiesClassLoader->register();

			//Paso 3: Cargamos las clases que sirven de proxies
			$proxiesClassLoader = new ClassLoader('proxies', dirname(__DIR__).'/model');
			$proxiesClassLoader->register();

			//Paso 4: Configuracion de la caché
			$config = new Configuration();
			$config->setMetadataCacheImpl(new \Doctrine\Common\Cache\ArrayCache());

			//Paso 5: Configuracion de los metadatos de la entidades
			$driverImpl = $config->newDefaultAnnotationDriver(array(dirname(__DIR__) . "/model/entities"));
			$config->setMetadataDriverImpl($driverImpl);

			//Paso 6: Configuracion clases proxies
			$config->setProxyDir(dirname(__DIR__) . '/model/proxies');
			$config->setProxyNamespace('proxies');
			$config->setAutoGenerateProxyClasses(true);

            //Paso 7: Configuración de los datos de conexión

            //si esta en la nube
            if (strcmp(gethostname(), 'ex-std-node3.prod.rhcloud.com') == 0) {
                $connectionOptions = array(
                        'driver' => 'pdo_pgsql',
                    	'host' => getenv('OPENSHIFT_POSTGRESQL_DB_HOST'),
                        'port' => getenv('OPENSHIFT_POSTGRESQL_DB_PORT'),
                        'dbname' => 'www',
                        'user' => 'admin',
                        'password' => 'J46qz1fjhxg7'
                );
            }
            else {
                $connectionOptions = array(
                    'driver' => 'pdo_pgsql',
                    //	'host' => 'localhost',
                    'dbname' => 'kurakura',
                    'user' => 'kurakura',
                    'password' => 'residenteresidente'
                );
            }

			//Pase 8: Creacion EntityManager
			self::$em = EntityManager::create($connectionOptions, $config);
		}

		return self::$em;
	}

}
?>